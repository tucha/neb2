@echo off


if "%1" equ "" (
	echo Usage: #-build.cmd ^<build-id^> [^<project-id^>]
	exit /b 1
) else (
	set BUILD_ID=%1
	set PROJECT_ID=%2
)


rem call 1-sync.cmd
rem if errorlevel 1 goto failure
call 2-label.cmd %BUILD_ID% %PROJECT_ID%
if errorlevel 1 goto failure
call 3-compile.cmd %BUILD_ID%
if errorlevel 1 goto failure
rem call 4-publish.cmd %BUILD_ID% %PROJECT_ID%
rem if errorlevel 1 goto failure


goto success


:success
echo.
echo ########################################
echo #                                      #
echo # Building done                        #
echo #                                      #
echo ########################################
exit /b 0
:failure
echo.
echo ########################################
echo #                                      #
echo # Building FAILED                      #
echo #                                      #
echo ########################################
exit /b 1
