#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include <QObject>
#include <QSharedMemory>
#include <QTimer>

#include "miscellaneous/parameters.h"

class ProcessManager : public QObject
{
	Q_OBJECT
public:
	explicit ProcessManager( QObject *parent, QString key );
	bool IsPrimaryProcess(void)const;
	bool IsSecondaryProcess(void)const;

	void StartListening(void);
	bool SendParameters( Parameters parameters );

signals:
	void SignalReceiveParameters( Parameters parameters );

private slots:
	void SlotListen();

private:
	QString ReadMemoryAndErase(void);
	bool WriteMemory( QString text );

	QSharedMemory m_memory;
	bool m_isPrimaryProcess;
	QTimer m_timer;
};

#endif // PROCESSMANAGER_H
