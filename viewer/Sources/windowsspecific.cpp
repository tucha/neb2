#ifndef OS_WINDOWS
#error "This file must be compiled on Windows OS only"
#endif

#include "windowsspecific.h"

#include <defines.h>

//extern "C"
//{
//	   int __stdcall ZwSetInformationThread(HANDLE,int,unsigned long*,unsigned long);
//	   int __stdcall ZwQueryInformationProcess(HANDLE,int,unsigned long*,unsigned long,unsigned long*);
//}

//#define ThreadHideFromDebugger 0x11
//#define ProcessDebugPort   0x7
//#define ProcessDebugObjectHandle 0x1E
//#define ProcessDebugFlags 0x1F

WindowsSpecific::WindowsSpecific()
{

}

WindowsSpecific::~WindowsSpecific()
{

}

DwordList WindowsSpecific::GetProcessThreads(qint64 processId)
{
	DwordList result;

	HANDLE hThreadSnap = CreateToolhelp32Snapshot( TH32CS_SNAPTHREAD, 0 );
	if( hThreadSnap == INVALID_HANDLE_VALUE ) return result;

	THREADENTRY32 te32;
	te32.dwSize = sizeof( THREADENTRY32 );
	if( Thread32First( hThreadSnap, &te32 ) == FALSE )
	{
		CloseHandle( hThreadSnap );
		return result;
	}

	while( true )
	{
		if( te32.th32OwnerProcessID == processId ) result.append( te32.th32ThreadID );
		if( Thread32Next(hThreadSnap, &te32 ) == FALSE ) break;
	}

	CloseHandle( hThreadSnap );
	return result;
}

HandleList WindowsSpecific::OpenThreads(DwordList threadIds)
{
	HandleList result;
	foreach( DWORD threadId, threadIds )
	{
		HANDLE handle = OpenThread( THREAD_SUSPEND_RESUME, false, threadId );
		if( handle == NULL ) continue;
		result.append( handle );
	}
	return result;
}

void WindowsSpecific::CloseThreads(HandleList threadHandles)
{
	foreach( HANDLE threadHandle, threadHandles )
	{
		CloseHandle( threadHandle );
	}
}

bool WindowsSpecific::IsThreadSuspended(DWORD threadId)
{
	HANDLE handle = OpenThread( THREAD_SUSPEND_RESUME, false, threadId );
	DWORD suspendCount = ResumeThread( handle );
	CloseHandle( handle );
	if( suspendCount == 0 ) return false;
	return true;
}

bool WindowsSpecific::IsThreadSuspended(HANDLE threadHandle)
{
	DWORD suspendCount = ResumeThread( threadHandle );
	if( suspendCount == 0 ) return false;
	return true;
}

void WindowsSpecific::KillProcess(qint64 processId)
{
	HANDLE handle = OpenProcess( PROCESS_TERMINATE, false, processId );
	TerminateProcess( handle, 0 );
	CloseHandle( handle );
}

//void WindowsSpecific::HideThreadsFromDebugger(HandleList threadHandles)
//{
//	foreach( HANDLE threadHandle, threadHandles )
//	{
//		ZwSetInformationThread( threadHandle, ThreadHideFromDebugger, 0, 0 );
//	}
//}
