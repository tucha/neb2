#include "thumbnail.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QPixmap>
#include <QProgressBar>
#include <QMouseEvent>
#include <QPainter>

QImage Thumbnail::g_bookmarkImage = QImage();
QImage Thumbnail::g_commentImage = QImage();

Thumbnail::Thumbnail(QWidget *parent) :
	QWidget(parent),
	m_request( NULL ),
	m_layout( new QVBoxLayout() ),
	m_label( new QLabel() ),
	m_pageIndex( new QLabel() ),
	m_progressBar( new QProgressBar() ),
	m_isLoaded( false ),
	m_bookmark( false ),
	m_comment(),
	m_image()
{
	m_label->setFrameStyle( QFrame::Panel );

	m_progressBar->hide();
	m_layout->addWidget( m_progressBar );
	m_layout->addWidget( m_label );
	m_layout->addWidget( m_pageIndex );

	m_layout->setAlignment( m_pageIndex, Qt::AlignCenter );

	this->setLayout( m_layout );

	Clear();
}

Thumbnail::~Thumbnail()
{
	Clear();
}

bool Thumbnail::IsEmpty() const
{
	return m_request == NULL;
}

void Thumbnail::Load(int pageIndex, Request* baseRequest)
{
	if( AlreadyLoaded( pageIndex, baseRequest ) ) return;
	Clear();

	RequestPage* tmp = baseRequest->NewPage( pageIndex );

	m_request = tmp->NewImageFixed( TYPE_THUMBNAILS, GEOMETRY_MAX, THUMBNAIL_SIZE, FORMAT_JPEG );
	connect(m_request,SIGNAL(SignalFinished(ErrorCode,QImage)),this,SLOT(SlotFinishedImage(ErrorCode,QImage)));
	connect(m_request,SIGNAL(SignalDownloadProgress(qint64,qint64)),this,SLOT(SlotDownloadProgress(qint64,qint64)));
	m_request->Send();

	tmp->deleteLater();
}

void Thumbnail::Clear()
{
	DeleteLaterAndNull( m_request );

	m_pageIndex->setText( QString() );
	m_label->setFrameStyle( QFrame::NoFrame );
	m_label->setPixmap( QPixmap() );
	m_progressBar->hide();
	m_isLoaded = false;
//	m_bookmark = false;
//	m_comment = QString();
	m_image = QImage();

	this->hide();
	this->resize( QSize() );
}

bool Thumbnail::IsLoaded() const
{
	return m_isLoaded;
}

void Thumbnail::mousePressEvent(QMouseEvent* event)
{
	UNUSED( event );

	if( m_request == NULL ) return;

	if( event->button() != Qt::LeftButton ) return;

	if( event->modifiers().testFlag( Qt::ControlModifier ) )
	{
		emit AddToSelection( PageIndex() - 1 );
	}
	else if ( event->modifiers().testFlag( Qt::ShiftModifier ) )
	{
		emit AddRangeToSelection( PageIndex() - 1 );
	}
	else emit GotoPage( PageIndex() );
}

void Thumbnail::SetBookmark(bool bookmark)
{
	if( m_bookmark == bookmark ) return;

	m_bookmark = bookmark;
	UpdateImage();
}

void Thumbnail::SetComment(QString comment)
{
	if( m_comment == comment ) return;

	m_comment = comment;
	UpdateImage();
}

int Thumbnail::PageIndex() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return -1;

	return m_request->PageIndex();
}

void Thumbnail::SlotFinishedImage(ErrorCode error, QImage image)
{
	if( IsEmpty() ) return;
	if( sender() != m_request ) return;

	if( error == QNetworkReply::ContentOperationNotPermittedError )
	{
		Clear();
		return;
	}

	if( error != API_NO_ERROR )
	{
		m_request->Send();
		return;
	}

	m_progressBar->hide();
	m_pageIndex->setText( QString::number( SENDER_PAGE->PageIndex() ) );
	m_image = image;
	m_isLoaded = true;
	UpdateImage();

	this->show();
	this->resize( sizeHint() );

	emit Loaded( PageIndex() );
}

void Thumbnail::SlotDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	if( IsEmpty() ) return;
	if( sender() != m_request ) return;

	m_progressBar->show();
	m_progressBar->setMaximum( bytesTotal );
	m_progressBar->setValue( bytesReceived );
	m_pageIndex->setText( QString::number( SENDER_PAGE->PageIndex() ) );

	this->show();
}

QImage Thumbnail::Image() const
{
	QImage result = m_image;

	float size = 23;
	float space = 4;

	if( m_bookmark )
	{
		QRectF place( result.width() - size - space, space, size, size );
		QPainter( &result ).drawImage( place, BookmarkImage() );
	}

	if( m_comment.isEmpty() == false )
	{
		QRectF place( result.width() - size - space, result.height() - size - space, size, size );
		QPainter( &result ).drawImage( place, CommentImage() );
	}

	return result;
}

void Thumbnail::UpdateImage()
{
	if( m_isLoaded == false ) return;

	m_label->setFrameStyle( FRAME_STYLE );
	m_label->setPixmap( QPixmap::fromImage( Image() ) );
}

bool Thumbnail::AlreadyLoaded(int pageIndex, Request* baseRequest) const
{
	if( IsEmpty() ) return false;
	if( PageIndex() != pageIndex ) return false;
	if( m_request->GetDocument() != baseRequest->GetDocument() ) return false;
	if( m_request->GetIdentifier() != baseRequest->GetIdentifier() ) return false;
	return true;
}

QImage Thumbnail::BookmarkImage()
{
	if( g_bookmarkImage.isNull() ) g_bookmarkImage = QImage( ":/icons/markbookmark" );
	return g_bookmarkImage;
}

QImage Thumbnail::CommentImage()
{
	if( g_commentImage.isNull() ) g_commentImage = QImage( ":/icons/markcomment" );
	return g_commentImage;
}
