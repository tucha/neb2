#ifndef TRANSLATIONVIEW_H
#define TRANSLATIONVIEW_H

#include <QWidget>
#include <QWebEngineView>
#include <QNetworkAccessManager>
#include <QOpenGLWidget>
#include <QPointer>

#include "document.h"

class TranslationView : public QWebEngineView
{
    Q_OBJECT
    QNetworkAccessManager* m_manager;

    bool eventFilter(QObject *obj, QEvent *event);
    QPointer<QOpenGLWidget> child_;
    QNetworkReply* m_lastReply;
public:
    TranslationView(QWidget *parent = 0);

    bool event(QEvent * ev);

private slots:
    void replyFinished(QNetworkReply * reply);
public slots:
    void tryShowTranslation(int pageIndex, Document document, QString token);
    void Clear();
signals:
    void wantToShow();
    void wantToHide();
    void translationSearchFinished();
};

#endif // TRANSLATIONVIEW_H
