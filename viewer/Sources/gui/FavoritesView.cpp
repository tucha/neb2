#include "FavoritesView.h"

#include <QVBoxLayout>
#include <QString>
#include <QWidget>
#include <QKeyEvent>
#include <QScrollBar>
#include <QAction>

#include "../miscellaneous/config.h"

#include <common.h>
#include <QJsonDocument>

#define SPACING 0
#define RECORDS_BATCH_COUNT 150
#define ICON_SIZE 70

FavoritesView::FavoritesView(QWidget *parent) :
    QListWidget(parent)
{
    setFrameStyle( QFrame::NoFrame );
    setAutoFillBackground( true );

//    setStyleSheet("QListWidget::item { border-bottom: 1px solid black; }" );
    setBackgroundRole( QPalette::Light );
    verticalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );
    horizontalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );

    setWordWrap(true);
    setTextElideMode(Qt::ElideNone);

    setIconSize(QSize(ICON_SIZE, ICON_SIZE));
    setSpacing(3);

    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, &QNetworkAccessManager::finished, this, &FavoritesView::replyFinished);

    connect(this, &FavoritesView::currentRowChanged, this, &FavoritesView::rowChanged);
    connect(this, &FavoritesView::itemClicked, this, &FavoritesView::onItemClicked);
    m_currentDocumentIndex = -1;
}

FavoritesView::~FavoritesView()
{
    Clear();
}

void FavoritesView::Load(Request* baseRequest)
{
    if(baseRequest == 0)
        return;
    m_perfCounter.start();
    m_currentDocumentId = baseRequest->GetDocument().DocumentId();
    addItem(createIndicator());
    m_token = baseRequest->GetIdentifier().GetUserToken();
    m_base = baseRequest->GetDocument().BaseUrl();
    QString templ = "%1/get_favorites_count?token=%2";

    auto url = QUrl(templ
                    .arg(m_base)
                    .arg(m_token)

                    );
    m_lastCountReply = m_manager->get(QNetworkRequest(url));
    Clear();
}

void FavoritesView::Clear()
{
   m_currentRecordsDownloaded = 0;
   m_scrolled = false;
   m_totalRecords = 0;
   clear();
   m_recordDataList.clear();
}

void FavoritesView::replyFinished(QNetworkReply *reply)
{
    reply->deleteLater();
    if(reply == m_lastCountReply)
    {

        bool ok = false;
        m_totalRecords = m_lastCountReply->readAll().toInt(&ok);
        if(ok && m_totalRecords > 0)
        {
            downloadRecords();
        }
        else
            Clear();
    }
    else if(reply == m_lastDataReply)
    {
        auto data = reply->readAll();
        auto d = QJsonDocument::fromJson(data);
        auto vl = d.toVariant().toList();
        QList<RecordData> recordList;
        for(auto r: vl)
        {
            RecordData record;
            record.author = r.toMap().value("author", "").toString();
            record.documentId = r.toMap().value("Id", "").toString();
            record.title = r.toMap().value("title", "").toString();
            record.icon = QImage::fromData(QByteArray::fromBase64(r.toMap().value("icon", "").toByteArray()), "png");
            if(m_currentDocumentId == record.documentId)
                m_currentDocumentIndex = m_recordDataList.size() + recordList.size();
            recordList << record;

        }
        m_recordDataList.append(recordList);
        m_currentRecordsDownloaded += vl.count();
        delete takeItem(count() - 1);
        FillList(recordList);
        if(m_currentRecordsDownloaded < m_totalRecords)
        {
            addItem(createIndicator());
            downloadRecords();
        }
        else
        {

            qDebug() << m_currentRecordsDownloaded << " favotites load time" << m_perfCounter.elapsed();
        }

        if(m_currentDocumentIndex != -1 && (!m_scrolled || true))
        {
            m_scrolled = true;
            scrollToItem(item(m_currentDocumentIndex), QAbstractItemView::PositionAtCenter);
            setCurrentRow(m_currentDocumentIndex);
            item(m_currentDocumentIndex)->setSelected(true);
        }
    }
}

QListWidgetItem *FavoritesView::createIndicator()
{
    auto loadingIndicator = new QListWidgetItem(tr("Loading..."), this);
    loadingIndicator->setTextAlignment(Qt::AlignHCenter);
    auto f = loadingIndicator->font();
    f.setPointSize(f.pointSize() * 1.5);
    loadingIndicator->setFont(f);
    return loadingIndicator;
}
void FavoritesView::rowChanged(int index)
{
    if(index == -1)
        return;

}

void FavoritesView::onItemClicked(QListWidgetItem *item)
{
    auto index = row(item);
    emit OpenDocument(Document::FromId(m_recordDataList[index].documentId));
}

void FavoritesView::downloadRecords()
{
    QString templ = "%1/get_favorites?token=%2&offset=%3&count=%4";

    auto url = QUrl(templ
                    .arg(m_base)
                    .arg(m_token)
                    .arg(m_currentRecordsDownloaded)
                    .arg(RECORDS_BATCH_COUNT)
                    );
    m_lastDataReply = m_manager->get(QNetworkRequest(url));
}

void FavoritesView::FillList(QList<RecordData> records)
{
    for(auto r: records)
    {
//        r.icon = QImage("/Users/Tucher/Downloads/tst.jpg");
//        r.author = "Fffuu";
        QListWidgetItem * newItem;

        QStringList textLines;
        if(!r.author.isEmpty())
            textLines << tr("Author: %1").arg(r.author);
        if(!r.title.isEmpty())
            textLines << tr("Title: %1").arg(r.title);

        if(r.icon.isNull())
        {
            newItem = new QListWidgetItem(textLines.join('\n'), this);
        }
        else
        {
            newItem = new QListWidgetItem(QIcon(QPixmap::fromImage(r.icon)), textLines.join('\n'), this);
        }
        if(count() % 2 == 1)
            newItem->setBackgroundColor(QColor(245, 245, 245));
        addItem(newItem);
    }


}



