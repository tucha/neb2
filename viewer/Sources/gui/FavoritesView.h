#ifndef FAVORITESVIEW_H
#define FAVORITESVIEW_H

#include <QObject>
#include <QWidget>

#include <QListWidget>
#include <QNetworkAccessManager>
#include <QStringList>
#include <QHBoxLayout>
#include <QLabel>

#include <document.h>

struct RecordData
{
    QString author;
    QString documentId;
    QString title;
    QImage icon;
};


class FavoritesView : public QListWidget
{
    Q_OBJECT
public:
    explicit FavoritesView(QWidget *parent = 0);
    ~FavoritesView();

    void Load(Request* baseRequest);
    void Clear(void);


signals:
    void OpenDocument( Document document );
    void DownloadingError( Document document );

private slots:

    void replyFinished(QNetworkReply * reply);
    void rowChanged(int index);
    void onItemClicked(QListWidgetItem* item);
private:
    int SelectedIndex(void) const;

    QNetworkAccessManager* m_manager;
    QNetworkReply* m_lastCountReply;
    QNetworkReply* m_lastDataReply;

    QString m_token;
    QString m_base;
    int m_totalRecords;
    int m_currentRecordsDownloaded;

    void downloadRecords();
    void FillList(QList<RecordData> records);

    QList<RecordData> m_recordDataList;

    QString m_currentDocumentId;
    int m_currentDocumentIndex;
    bool m_scrolled;
    QListWidgetItem * createIndicator();

    QTime m_perfCounter;
};


#endif // FAVORITESVIEW_H
