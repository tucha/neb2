#ifndef COMMENTWINDOW_H
#define COMMENTWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QPushButton>

class CommentWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit CommentWindow(QWidget *parent = 0);

	void SetText( QString text );

signals:
	void SetComment( QString text );

protected:
	virtual void keyPressEvent(QKeyEvent * event);

private slots:
	void Save();
	void Delete();
	void Cancel();
	void TextChanged();

private:
	void Translate(void);

	QWidget* m_centralWidget;
	QVBoxLayout* m_verticalLayout;
	QHBoxLayout* m_horizontalLayout;
	QLabel* m_label;
	QTextEdit* m_textEdit;
	QPushButton* m_saveButton;
	QPushButton* m_deleteButton;
	QPushButton* m_cancelButton;
	QString m_originalText;
};

#endif // COMMENTWINDOW_H
