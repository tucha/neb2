#include "pagesetwindow.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

PageSetWindow::PageSetWindow(QWidget *parent) :
	QDialog(parent),

	m_verticalLayout( new QVBoxLayout() ),
	m_horizontalLayout( new QHBoxLayout() ),
	m_label( new QLabel() ),
	m_lineEdit( new QLineEdit() ),
	m_okButton( new QPushButton() ),
	m_cancelButton( new QPushButton() )
{
	CreateGui();
	ConnectSlots();
	Translate();
}

void PageSetWindow::SetPageSet(PageSet pageSet)
{
	m_lineEdit->setText( pageSet.ToString() );
}

PageSet PageSetWindow::GetPageSet() const
{
	if( PageSet::CheckString( m_lineEdit->text() ) == false ) return PageSet();
	return PageSet::FromString( m_lineEdit->text() );
}

void PageSetWindow::SetTitle(QString title)
{
	this->setWindowTitle( title );
}

void PageSetWindow::SetLabel(QString label)
{
	m_label->setText( label );
	AdjustSize();
}

void PageSetWindow::SetOkButton(QString okButton)
{
	m_okButton->setText( okButton );
}

void PageSetWindow::TextChanged(QString text)
{
	DebugAssert( sender() == m_lineEdit );
	m_okButton->setEnabled( PageSet::CheckString( text ) );
}

void PageSetWindow::CreateGui()
{
	m_verticalLayout->addWidget( m_label );
	m_verticalLayout->addWidget( m_lineEdit );
	m_verticalLayout->addLayout( m_horizontalLayout );
	m_horizontalLayout->addWidget( m_okButton );
	m_horizontalLayout->addWidget( m_cancelButton );

	m_label->setWordWrap( true );

	this->setLayout( m_verticalLayout );
	this->setWindowFlags( Qt::Window | Qt::WindowCloseButtonHint );
	AdjustSize();
}

void PageSetWindow::Translate()
{
	m_cancelButton->setText( tr( "Cancel" ) );
}

void PageSetWindow::ConnectSlots()
{
	connect(m_okButton,SIGNAL(clicked()),this,SLOT(accept()));
	connect(m_cancelButton,SIGNAL(clicked()),this,SLOT(reject()));
	connect(m_lineEdit,SIGNAL(textChanged(QString)),this,SLOT(TextChanged(QString)));
	connect(m_lineEdit,SIGNAL(returnPressed()),m_okButton,SLOT(click()));
}

void PageSetWindow::AdjustSize()
{
	this->setFixedSize( 300, sizeHint().height() );
}
