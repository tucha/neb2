#ifndef USERPASSWORDDIALOG_H
#define USERPASSWORDDIALOG_H

#include <QDialog>
#include <QString>

class QVBoxLayout;
class QHBoxLayout;
class QLabel;
class QLineEdit;
class QPushButton;
class QAuthenticator;

class Config;

#include <common.h>

class UserPasswordDialog : public QDialog
{
	Q_OBJECT
public:
	explicit UserPasswordDialog(QWidget *parent = 0);

	QDialog::DialogCode Show(void);

	QString User(void)const;
	QString Password(void)const;


private:
	void CreateGui();
	void Translate();
	void ConnectSlots();

	QVBoxLayout*	m_verticalLayout;
	QHBoxLayout*	m_horizontalLayout;
	QLabel*			m_labelUser;
	QLineEdit*		m_lineEditUser;
	QLabel*			m_labelPassword;
	QLineEdit*		m_lineEditPassword;
	QPushButton*	m_okButton;
	QPushButton*	m_cancelButton;
};

#endif // USERPASSWORDDIALOG_H
