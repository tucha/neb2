#ifndef PAGESETWINDOW_H
#define PAGESETWINDOW_H

#include <QDialog>

#include <pageset.h>

class QVBoxLayout;
class QHBoxLayout;
class QLabel;
class QLineEdit;
class QPushButton;

class PageSetWindow : public QDialog
{
	Q_OBJECT
public:
	explicit PageSetWindow(QWidget *parent = 0);

	void SetPageSet( PageSet pageSet );
	PageSet GetPageSet( void )const;

	void SetTitle( QString title );
	void SetLabel( QString label );
	void SetOkButton( QString okButton );

private slots:
	void TextChanged(QString text);

private:
	void CreateGui();
	void Translate();
	void ConnectSlots();
	void AdjustSize();

	QVBoxLayout*	m_verticalLayout;
	QHBoxLayout*	m_horizontalLayout;
	QLabel*			m_label;
	QLineEdit*		m_lineEdit;
	QPushButton*	m_okButton;
	QPushButton*	m_cancelButton;
};

#endif // PAGESETWINDOW_H
