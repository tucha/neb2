#include "translationview.h"
#include "logic.h"
#include <QWidget>
#include <QEvent>
#include <QPaintEvent>
#include <QJsonDocument>
#include <QWebEngineScriptCollection>
#include <QWebEngineScript>

bool TranslationView::eventFilter(QObject *obj, QEvent *event)
{
////    qDebug() << event->type();
//    return false;
//    if (obj == child_ /*&& event->type() == QEvent::MouseButtonPress*/) {
//        qDebug() << event->type();
//        return false;
////        QPaintEvent *pe = static_cast<QPaintEvent*>(event);
//        // do something with paint event
//        // ...
//        // or just emit signal to notify other objects
////        emit delegatePaint(pe);
//    }

    return QWebEngineView::eventFilter(obj, event);
}

TranslationView::TranslationView(QWidget *parent):
    QWebEngineView(parent)
{
    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, &QNetworkAccessManager::finished, this, &TranslationView::replyFinished);

    QWebEngineScript script;
    script.setInjectionPoint(QWebEngineScript::DocumentReady);
    script.setWorldId(QWebEngineScript::MainWorld);
    script.setSourceCode("function disableselect(e) {return false};document.onselectstart = new Function (\"return false\"); document.onmousedown = disableselect");

    page()->scripts().insert(script);
}

bool TranslationView::event(QEvent *ev)
{
    if (ev->type() == QEvent::ChildAdded) {
        QChildEvent *child_ev = static_cast<QChildEvent*>(ev);

        // there is also QObject child that should be ignored here;
        // use only QOpenGLWidget child
        QOpenGLWidget *w = qobject_cast<QOpenGLWidget*>(child_ev->child());
        if (w) {
            child_ = w;
            w->installEventFilter(this);
        }
    }

    return QWebEngineView::event(ev);
}

void TranslationView::replyFinished(QNetworkReply *reply)
{
    reply->deleteLater();
    if(m_lastReply != reply)
        return;
    auto data = reply->readAll();
    auto js = QJsonDocument::fromJson(data).toVariant().toMap();

    if(js["flagTranslate"].toInt() == 1)
    {
        auto html = js["translate"].toString();
        setHtml(html);
        qDebug() << html;
        emit wantToShow();        
    }
    else
    {
        Clear();
    }
    emit translationSearchFinished();
}

void TranslationView::tryShowTranslation(int pageIndex, Document document, QString token)
{
    QString templ = "%1/%2/document/%3/translate?token=%4&type=plain";
    auto url = QUrl(templ
                    .arg(document.BaseUrl())
                    .arg(document.DocumentId())
                    .arg(pageIndex)
                    .arg(token)
                    );
    m_lastReply = m_manager->get(QNetworkRequest(url));
}

void TranslationView::Clear()
{
    setHtml("");
    hide();
}
