#ifndef THUMBNAIL_H
#define THUMBNAIL_H

#include <QWidget>
#include <common.h>

class QVBoxLayout;
class QLabel;
class QProgressBar;

#define THUMBNAIL_SIZE 100
#define FRAME_STYLE QFrame::Panel

class Thumbnail : public QWidget
{
	Q_OBJECT
private:
	DISABLE_COPY( Thumbnail )

public:
	explicit Thumbnail( QWidget *parent = NULL );
	~Thumbnail();

	bool IsEmpty(void)const;
	void Load(int pageIndex, Request* baseRequest );
	void Clear(void);
	bool IsLoaded(void)const;

	virtual void mousePressEvent(QMouseEvent* event);

	void SetBookmark( bool bookmark );
	void SetComment(QString comment );

	int PageIndex(void)const;

signals:
	void GotoPage( int pageIndex );
	void AddToSelection( int pageIndex );
	void AddRangeToSelection( int pageIndex );
	void Loaded( int pageIndex );

private slots:
	void SlotFinishedImage( ErrorCode error, QImage image );
	void SlotDownloadProgress( qint64 bytesReceived, qint64 bytesTotal );

private:
	QImage Image(void)const;
	void UpdateImage(void);
	bool AlreadyLoaded(int pageIndex, Request* baseRequest)const;

	RequestImageFixed* m_request;
	QVBoxLayout* m_layout;
	QLabel* m_label;
	QLabel* m_pageIndex;
	QProgressBar* m_progressBar;
	bool m_isLoaded;
	bool m_bookmark;
	QString m_comment;
	QImage m_image;

	static QImage BookmarkImage(void);
	static QImage CommentImage(void);
	static QImage g_bookmarkImage;
	static QImage g_commentImage;
};

#endif // THUMBNAIL_H
