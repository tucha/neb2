#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDateTime>
#include <QCheckBox>
class Display;
class Thumbnails;
class SearchPanel;
class DebugPanel;
class PagesControl;
class ZoomPanel;
class Renderer;
class PageSetWindow;
class CommentWindow;
class FavoritesManager;
class BookmarksControl;
class History;
class Description;

class QToolBar;
class QSplitter;
class QTabWidget;
class QAction;
class QActionGroup;
class QMessageBox;
class Logic;
class TranslationView;
class FavoritesView;
#include <common.h>
#include <pageset.h>
#include <document.h>
#include <bookmarksmanager.h>
#include <commentsmanager.h>
#include <viewersettings.h>

#include "../miscellaneous/documentstate.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT
private:
	DISABLE_COPY( MainWindow )

public:
	MainWindow(QWidget *parent, Logic* owner);
	~MainWindow();

	bool IsEmpty(void)const;
	void Load( Request* baseRequest, AccountBaseRequest* accountBase, DocumentState state );
	void Clear(void);

	DocumentState GetDocumentState(void)const;

//	History* GetFavorite(void)const;
    FavoritesView* GetFavorite(void)const;
	History* GetHistory(void)const;

public slots:
	void UpdateFavorites( FavoritesManager* favorites );

signals:
	void SaveState( DocumentState document );
	void SaveDocument( Document document, PageSet pageSet );
	void PrintDocument( Document document, PageSet pageSet );
	void SendFavoritesRequest( Document document, bool favorite );
	void OpenDocument( Document document );
	void DownloadingError( Document document );
	void DownloadingSuccess( Document document );

protected:
	virtual void closeEvent( QCloseEvent* event );
	virtual void wheelEvent( QWheelEvent* event );
	virtual QMenu* createPopupMenu();
	virtual void keyPressEvent(QKeyEvent * event);
	virtual void resizeEvent( QResizeEvent* event );

private slots:
	// Private slots
	void SlotFinishedPagesCount( ErrorCode error, int pagesCount );
	void GotoPage( int pageIndex );
	void GotoSearch( int pageIndex, WordCoordinates coordinates );
	void NextPage();
	void PrevPage();
	void FirstPage();
	void LastPage();
    void autoTurnChanged(bool v);
	void ScaleSelected( int scale );
	void ScaleFullToggled(bool checked);
	void ScaleWidthToggled(bool checked);
	void FullScreen(bool enabled);
	void PrintPages();
	void SavePages();
	void Buy();
	void BookmarksUpdate( BookmarksManager* manager );
	void CommentsUpdate( CommentsManager* manager );
	void TriggerBookmark(bool checked);
	void TriggerComment();
	void SetComment( QString text );
	void TriggerFavorites(bool checked);
	void ShowCard();
	void Open();
	void SecondWarningDialogFinished(int result);
    void timeForNextPage();
private:
	// Private methods
	void CreateActions(void);
	void CreateGui(void);
	void ConnectSlots(void);
	void Translate(void);
	void PrepareWindow(void);
	void RestoreDocumentState(bool restorePageIndex);
	void CreateWarningDialogs(void);
	void DeleteWarningDialogs(void);
	void HideWarningDialogs(void);
	void ApplyHidePages(void);
	void ShowSecondWarning(void);
	bool CheckSecondWarningInterval(void);
	void ApplySecondWarningGrow(void);
	void ShowSecondWarningInternal(void);
	void ShowFirstWarning();
	void ShowPendingRequestMessage(void);
	void ShowPageIsUnavailableMessage(void);
	Logic*			Owner(void)const;
	ViewerSettings	Settings(void)const;
	int				PageIndex(void)const;
	void			EnableGui(void);
	void			DisableGui(void);
	void			SetBookmarksTooltip( bool checked );
	void			SetFavoritesTooltip( bool checked );
	void			SetCommentsTooltip( bool checked );

	// Actions
	QAction*	m_actionZoomIn;
	QAction*	m_actionZoomOut;
	QAction*	m_actionRor;
	QAction*	m_actionRol;
	QAction*	m_actionFavorite;
	QAction*	m_actionBuy;
	QAction*	m_actionFullScreen;

    void setupAutoscroll();
#ifdef PRINT_SAVE
	QAction*	m_actionCopy;
#endif
	QAction*	m_actionScaleWidth;
	QAction*	m_actionScaleFull;
	QAction*	m_actionNextPage;
	QAction*	m_actionFirstPage;
	QAction*	m_actionPrevPage;
	QAction*	m_actionLastPage;
    QCheckBox*	m_actionAutoTurn;
	QAction*	m_actionBookmark;
	QAction*	m_actionComment;
#ifdef PRINT_SAVE
	QAction*	m_actionPrint;
#endif
	QAction*	m_actionInvert;
	QAction*	m_actionInfo;
//	QAction*	m_actionOpen;

	QActionGroup* m_groupScaling;
#ifdef PRINT_SAVE
	QActionGroup* m_groupDocument;
#endif
	QActionGroup* m_groupNavigation;
	QActionGroup* m_groupView;
	QActionGroup* m_groupPrivate;
	QActionGroup* m_groupOthers;

	QToolBar*		m_toolBarScaling;
#ifdef PRINT_SAVE
	QToolBar*		m_toolBarDocument;
#endif
	QToolBar*		m_toolBarNavigation;
	QToolBar*		m_toolBarView;
	QToolBar*		m_toolBarPrivate;
	QToolBar*		m_toolBarOthers;

	// Gui item
	QSplitter*		m_guiSplitter;
	QTabWidget*		m_guiTabWidget;

	Display*			m_display;

    TranslationView* m_translationView;
#ifndef DISABLE_THUMBNAILS
	Thumbnails*			m_thumbnails;
#endif
	SearchPanel*		m_searchPanel;
	DebugPanel*			m_debugPanel;
	PagesControl*		m_pagesControl;
	ZoomPanel*			m_zoomPanel;
	PageSetWindow*		m_pageSetWindow;
	BookmarksControl*	m_bookmarksControl;
	BookmarksControl*	m_commentsControl;

	Renderer*			m_renderer;
	RequestPagesCount*	m_request;
	User				m_user;
	int					m_pagesCount;
	Qt::WindowStates	m_previousState;

	DocumentState		m_state;

	BookmarksManager	m_bookmarksManager;
	CommentsManager		m_commentsManager;

	CommentWindow*		m_commentWindow;

//	History*	m_favorite;
    FavoritesView*	m_favorite;
	History*	m_history;

	Description*		m_description;

	QMessageBox*		m_firstWarningDialog;
	QMessageBox*		m_secondWarningDialog;
	int					m_secondWarningResult;
	QMessageBox*		m_pendingRequestMessage;

	Logic* const		m_owner;

	int					m_secondWarningPagesLeft;
	QDateTime			m_secondWarningLastTime;
	int					m_secondWarningInterval;
	bool				m_secondWarningShowOnlyOnce;

	QMessageBox*		m_pageIsUnavailableMessage;

    bool m_firstOpening;

    QTimer m_autoScrollTimer;
};

#endif // MAINWINDOW_H
