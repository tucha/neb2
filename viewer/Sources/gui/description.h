#ifndef DESCRIPTION_H
#define DESCRIPTION_H

#include <QDialog>

#include <common.h>
#include <document.h>

class Record;
class QFormLayout;
class QPushButton;

class Description : public QDialog
{
	Q_OBJECT
public:
	explicit Description(QWidget *parent = 0);
	~Description();

	void Load( Request* baseRequest );
	void Clear(void);

	void Translate(void);

private:
	void CreateGui(void);

	QFormLayout* m_layout;

	Record* m_recordTitle;
	Record* m_recordAuthor;
	Record* m_recordPlace;
	Record* m_recordPublisher;
	Record* m_recordYear;
	Record* m_recordDescription;
	Record* m_recordISBN;
	Record* m_recordUDK;
	Record* m_recordBBK;
	Record* m_recordSign;
	Record* m_recordAnnotation;
};

#endif // DESCRIPTION_H
