#ifndef ZOOMPANEL_H
#define ZOOMPANEL_H

#include <QWidget>
#include <QList>

class QComboBox;
class QLabel;
class QHBoxLayout;

#include <defines.h>

class ZoomPanel : public QWidget
{
	Q_OBJECT
private:
	DISABLE_COPY( ZoomPanel )

public:
	explicit ZoomPanel(QWidget *parent = 0);
	void Translate(void);
	int CurrentScale(void)const;
	void SetCurrentScale(int value);

signals:
	void ScaleSelected(int scale);
	void SetZoomInEnabled(bool value);
	void SetZoomOutEnabled(bool value);

public slots:
	void ZoomIn(void);
	void ZoomOut(void);
	void ImplicitScaleChanged( int value );

private slots:
	void Selected(int index);
	void IndexChanged(int index);

private:
	QList<int> CreateZoomLevels(void);

	QHBoxLayout*	m_layout;
	QLabel*			m_label;
	QComboBox*		m_comboBox;
	QList<int>		m_zoomLevels;

	bool			m_internalIndexChange;
};

#endif // ZOOMPANEL_H
