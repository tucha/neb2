#include "searchpanel.h"
#include "searchresults.h"

#include <QBoxLayout>
#include <QLineEdit>
#include <QListView>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QMessageBox>

SearchPanel::SearchPanel(QWidget *parent) :
	QWidget(parent),
	m_baseRequest( NULL ),
	m_pagesCount( 0 )
{
	CreateGui();
	ConnectSlots();
	Translate();
}

SearchPanel::~SearchPanel()
{
	Clear();
}

void SearchPanel::Translate()
{
	m_label->setText( tr( "Text for search:" ) );
	m_groupBox->setTitle( tr( "" ) );
	m_searchButton->setText( tr( "Search" ) );
	m_clearButton->setText( tr( "Clear" ) );
}

void SearchPanel::Load(int pagesCount, Request* baseRequest)
{
	DebugAssert( pagesCount > 0 );
	if( pagesCount <= 0 ) return;

	m_baseRequest = baseRequest;
	m_pagesCount = pagesCount;
}

void SearchPanel::Clear()
{
	m_baseRequest = NULL;
	m_pagesCount = 0;
	m_textEdit->setText( "" );
	m_status->setText( tr( "Search results:" ) );
}

void SearchPanel::Select(int pageIndex)
{
	m_searchResults->Select( pageIndex );
}

void SearchPanel::Search()
{
	QString text = m_textEdit->text();
	if( text.isEmpty() ) return;
	foreach( QChar symbol, text )
	{
		if( symbol.isSpace() ) continue;
		if( symbol.isLetterOrNumber() ) continue;

		QString title = tr( "UncorrectSearchTitle" );
		QString message = tr( "UncorrectSearchMessage" );
		QMessageBox::warning( NULL, title, message );
		return;
	}

	if( m_baseRequest == NULL ) return;
	m_searchResults->Load( m_pagesCount, m_baseRequest, m_textEdit->text() );
	m_status->setText( tr( "Searching..." ) );
}

void SearchPanel::TextChanged(QString text)
{
	m_searchButton->setEnabled( text.isEmpty() == false );
	m_clearButton->setEnabled( text.isEmpty() == false );
	if( text.isEmpty() ) m_searchResults->Clear();
}

void SearchPanel::SlotGotoSearch(int pageIndex, WordCoordinates coordinates)
{
	emit GotoSearch( pageIndex, coordinates );
}

void SearchPanel::SearchFinished()
{
	m_status->setText( tr( "Searching was finished" ) );
}

void SearchPanel::NothingWasFound()
{
	m_status->setText( tr( "Nothing was found" ) );
}

void SearchPanel::SearchProgress(int received, int sent)
{
	m_status->setText( tr( "Searching %1 percents where finished" ).arg( 100 * received / sent  ) );
}

void SearchPanel::CreateGui()
{
	m_label = new QLabel();
	m_textEdit = new QLineEdit();
	m_searchButton = new QPushButton();
	m_groupBox = new QGroupBox();

	m_status = new QLabel();
	m_clearButton = new QPushButton();
	m_searchResults = new SearchResults();

	m_label->setMargin( 5 );
	m_searchButton->setEnabled( false );
	m_clearButton->setEnabled( false );

#ifdef OS_MACOSX
	m_textEdit->setMinimumWidth( 60 );
#else
	m_searchButton->setMinimumWidth( 40 );
#endif

	QBoxLayout* tmp = new QBoxLayout( QBoxLayout::LeftToRight );
	tmp->setMargin( 0 );
	tmp->setContentsMargins( 0, 0, 0, 0 );
	tmp->setSpacing( 0 );

	tmp->addWidget( m_label );
	tmp->addWidget( m_textEdit );
	tmp->addWidget( m_searchButton );
	//tmp->addWidget( m_clearButton );
	m_groupBox->setLayout( tmp );

	m_verticalLayout = new QBoxLayout( QBoxLayout::TopToBottom );
	m_verticalLayout->addWidget( m_groupBox );
	m_verticalLayout->addWidget( m_status );
	m_verticalLayout->addWidget( m_searchResults );

	this->setLayout( m_verticalLayout );
}

void SearchPanel::ConnectSlots()
{
	connect(m_searchButton,SIGNAL(clicked()),this,SLOT(Search()));
	connect(m_textEdit,SIGNAL(textChanged(QString)),this,SLOT(TextChanged(QString)));
	connect(m_searchResults,SIGNAL(GotoSearch(int,WordCoordinates)),this,SLOT(SlotGotoSearch(int,WordCoordinates)));
	connect(m_searchResults,SIGNAL(SearchFinished()),this,SLOT(SearchFinished()));
	connect(m_searchResults,SIGNAL(NothingWasFound()),this,SLOT(NothingWasFound()));
	connect(m_searchResults,SIGNAL(Progress(int,int)),this,SLOT(SearchProgress(int,int)));
	connect(m_clearButton,SIGNAL(clicked()),m_textEdit,SLOT(clear()));
	connect(m_textEdit,SIGNAL(returnPressed()),this,SLOT(Search()));
}
