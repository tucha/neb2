#include "history.h"

#include <QVBoxLayout>
#include <QString>
#include <QWidget>
#include <QKeyEvent>
#include <QScrollBar>
#include <QAction>

#include "historyrecord.h"
#include "../miscellaneous/config.h"

#include <common.h>

#define SPACING 0

History::History(QWidget *parent) :
	QScrollArea(parent),
	m_layout( new QVBoxLayout( this ) ),
	m_widget( new QWidget( this ) ),
	m_actionRemove( new QAction( this ) )
{
	m_widget->setLayout( m_layout );
	m_widget->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
	m_layout->setContentsMargins( SPACING,SPACING,SPACING,SPACING);
	m_layout->setSpacing( SPACING);
	this->setWidgetResizable( true );
	this->setWidget( m_widget );
	this->setFrameStyle( QFrame::NoFrame );
	this->setAutoFillBackground( true );
	this->setBackgroundRole( QPalette::Light );
	this->verticalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );
	this->horizontalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );

	connect(m_actionRemove,SIGNAL(triggered()),this,SLOT(ActionRemoveTriggered()));
	m_actionRemove->setText( tr( "delete" ) );
}

History::~History()
{
	Clear();
}

void History::Load(Request* baseRequest, DocumentList documents, Config* config)
{
	UNUSED( config );

	documents = Reverse( documents );
	while( Count() < documents.count() ) Add( NewRecord() );

	OptimizeRecords( documents );

	for( int i = 0; i < documents.count(); ++i )
	{
		Document document = documents.at( i );

		int pageIndex = 1;
//		if( config->GetState( document ).IsCorrect() ) pageIndex = config->GetState( document ).GetPageIndex();

		Request* requestDocument = baseRequest->New( document );
		RequestPage* requestPage = requestDocument->NewPage( pageIndex );

		Get( i )->Load( requestPage );

		DeleteLaterAndNull( requestPage );
		DeleteLaterAndNull( requestDocument );

		if( document == baseRequest->GetDocument() )
		{
			Get( i )->SetSelected( true );
			ensureWidgetVisible( Get( i ) );
		}
		else Get( i )->SetSelected( false );
	}

	while( Count() > documents.count() )
	{
		HistoryRecord* record = Get( Count() - 1 );
		Remove( record );
		record->deleteLater();
	}
}

void History::Clear()
{
	for( int i = 0; i < Count(); ++i )
	{
		Get( i )->Clear();
	}
}

void History::RemoveDocument(Document document)
{
	HistoryRecord* record = NULL;
	int index = -1;
	for( int i = 0; i < Count(); ++i )
	{
		if( Get( i )->GetDocument() == document )
		{
			record = Get( i );
			index = i;
			break;
		}
	}
	DebugAssert( record != NULL );
	if( record == NULL ) return;

	Remove( record );
	record->deleteLater();

	if( Count() == 0 ) return;
	if( index >= Count() ) index = Count() - 1;
	RecordClicked( Get( index ) );
}

void History::keyPressEvent(QKeyEvent* event)
{
	if( event->key() == Qt::Key_Down )
	{
		int index = SelectedIndex();
		if( index == -1 ) return;
		index += 1;
		if( index == Count() ) index = 0;
		RecordClicked( Get( index ) );
	}
	if( event->key() == Qt::Key_Up )
	{
		int index = SelectedIndex();
		if( index == -1 ) return;
		index -= 1;
		if( index == -1 ) index = Count() - 1;
		RecordClicked( Get( index ) );
	}
	if( event->key() == Qt::Key_Return )
	{
		int index = SelectedIndex();
		if( index == -1 ) return;
		emit OpenDocument( Get( index )->GetDocument() );
	}
	if( event->key() == Qt::Key_Delete )
	{
		m_actionRemove->trigger();
	}
}

void History::RecordClicked(HistoryRecord* record)
{
	DebugAssert( record != NULL );
	if( record == NULL ) return;

	for( int i = 0; i < Count(); i++ )
	{
		HistoryRecord* record = Get( i );
		record->SetSelected( false );
	}
	record->SetSelected( true );
	ensureWidgetVisible( record );
}

void History::ActionRemoveTriggered()
{
	int index = SelectedIndex();
	if( index == -1 ) return;
	emit RequestRemoval( Get( index )->GetDocument() );
}

HistoryRecord*History::NewRecord()
{
	HistoryRecord* result = new HistoryRecord( this );

	connect(result,SIGNAL(MouseClicked(HistoryRecord*)),this,SLOT(RecordClicked(HistoryRecord*)));
	connect(result,SIGNAL(DownloadingError(Document)),this,SIGNAL(DownloadingError(Document)));
	connect(result,SIGNAL(OpenDocument(Document)),this,SIGNAL(OpenDocument(Document)));

	result->addAction( m_actionRemove );
	result->setContextMenuPolicy( Qt::ActionsContextMenu );


	return result;
}

void History::Add(HistoryRecord* record)
{
	m_layout->addWidget( record );
}

void History::Remove(HistoryRecord* record)
{
	m_layout->removeWidget( record );
}

int History::Count() const
{
	return m_layout->count();
}

HistoryRecord*History::Get(int index) const
{
	DebugAssert( index >= 0 && index < Count() );
	return static_cast<HistoryRecord*>( m_layout->itemAt( index )->widget() );
}

int History::SelectedIndex() const
{
	for( int i = 0; i < Count(); ++i )
	{
		if( Get( i )->GetSelected() == true ) return i;
	}
	return -1;
}

DocumentList History::Reverse(DocumentList documents) const
{
	DocumentList result;

	for( int i = documents.count() - 1; i >= 0; --i )
	{
		result.append( documents.value( i ) );
	}

	return result;
}

void History::OptimizeRecords(DocumentList needDocuments)
{
	QHash<Document,int> documentHash = CollectDocumentHash();
	for( int i = 0; i < needDocuments.count(); ++i )
	{
		Document document = needDocuments.value( i );
		if( documentHash.contains( document ) == false ) continue;

		int documentIndexHave = documentHash.value( document );
		if( documentIndexHave == i ) continue;

		Swap( documentIndexHave, i );
		UpdateDocumentHash( documentHash, documentIndexHave, i );
	}
}

QHash<Document, int> History::CollectDocumentHash() const
{
	QHash<Document,int> result;

	for( int i = 0; i < Count(); ++i )
	{
		HistoryRecord* record = Get( i );
		if( record->IsEmpty() ) continue;
		result.insert( record->GetDocument(), i );
	}

	return result;
}

void History::Swap(int index1, int index2)
{
	if( index1 > index2 ) std::swap( index1, index2 );

	HistoryRecord* record1 = Get( index1 );
	HistoryRecord* record2 = Get( index2 );

	m_layout->removeWidget( record1 );
	m_layout->removeWidget( record2 );

	m_layout->insertWidget( index1, record2 );
	m_layout->insertWidget( index2, record1 );
}

void History::UpdateDocumentHash(QHash<Document, int>& documentHash, int index1, int index2) const
{
	HistoryRecord* record1 = Get( index1 );
	HistoryRecord* record2 = Get( index2 );

	if( record1->IsEmpty() == false )
	{
		Document document1 = record1->GetDocument();
		if( record2->IsEmpty() == false )
		{
			Document document2 = record2->GetDocument();
			DebugAssert( documentHash.value( document1 ) == index2 );
			DebugAssert( documentHash.value( document2 ) == index1 );
			documentHash[ document1 ] = index1;
			documentHash[ document2 ] = index2;
		}
		else
		{
			DebugAssert( documentHash.value( document1 ) == index2 );
			documentHash[ document1 ] = index1;
		}
	}
	else
	{
		if( record2->IsEmpty() == false )
		{
			Document document2 = record2->GetDocument();
			DebugAssert( documentHash.value( document2 ) == index1 );
			documentHash[ document2 ] = index2;
		}
		else
		{
			NotImplemented();
		}
	}

	DebugAssert( TestDocumentHash( documentHash ) );
}

bool History::TestDocumentHash(const QHash<Document, int>& documentHash) const
{
	int counter = 0;
	for( int i = 0; i < Count(); ++i )
	{
		HistoryRecord* record = Get( i );
		if( record->IsEmpty() ) continue;

		counter += 1;
		if( documentHash.value( record->GetDocument() ) != i ) return false;
	}
	if( counter != documentHash.count() ) return false;
	return true;
}
