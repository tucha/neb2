#include "zoompanel.h"

#include <QLabel>
#include <QComboBox>
#include <QHBoxLayout>

#include <limits>

#include <defines.h>

ZoomPanel::ZoomPanel(QWidget *parent) :
	QWidget(parent),
	m_layout( new QHBoxLayout( this ) ),
	m_label( new QLabel( this ) ),
	m_comboBox( new QComboBox( this ) ),
	m_zoomLevels( CreateZoomLevels() ),
	m_internalIndexChange( false )
{
	m_layout->addWidget( m_label );
	m_layout->addWidget( m_comboBox );

	this->setLayout( m_layout );
	this->setContentsMargins( 0,0,0,0 );
	this->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );

	Translate();

	connect(m_comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(IndexChanged(int)));
	connect(m_comboBox,SIGNAL(activated(int)),this,SLOT(Selected(int)));

	m_comboBox->setCurrentIndex( m_zoomLevels.indexOf( 100 ) );
}

void ZoomPanel::Translate()
{
	m_label->setText( tr( "Scale" ) );

	for( int i = 0; i < m_zoomLevels.count(); ++i )
	{
		int zoomLevel = m_zoomLevels.at( i );
		while( m_comboBox->count() <= i ) m_comboBox->addItem( "" );

		m_comboBox->setItemData( i, zoomLevel );
		m_comboBox->setItemText( i, tr( "%1%" ).arg( zoomLevel ) );
	}
	while( m_comboBox->count() > m_zoomLevels.count() ) m_comboBox->removeItem( m_comboBox->count() - 1 );
}

int ZoomPanel::CurrentScale() const
{
	int index = m_comboBox->currentIndex();

	DebugAssert( index >= 0 && index < m_zoomLevels.count() );
	if( index < 0 || index >= m_zoomLevels.count() ) return 100;

	return m_zoomLevels.at( index );
}

void ZoomPanel::SetCurrentScale(int value)
{
	int index = m_zoomLevels.indexOf( value );
	if( index == -1 ) return;
	m_comboBox->setCurrentIndex( index );
}

void ZoomPanel::ZoomIn()
{
	int index = m_comboBox->currentIndex();
	index += 1;
	if( index >= m_comboBox->count() ) return;

	m_comboBox->setCurrentIndex( index );
}

void ZoomPanel::ZoomOut()
{
	int index = m_comboBox->currentIndex();
	index -= 1;
	if( index < 0 ) return;

	m_comboBox->setCurrentIndex( index );
}

void ZoomPanel::ImplicitScaleChanged(int value)
{
	int minDelta = std::numeric_limits<int>::max();
	int minDeltaIndex = -1;
	for( int i = 0; i < m_zoomLevels.count(); i++ )
	{
		int zoomLevel = m_zoomLevels.at( i );
		int delta = qAbs( zoomLevel - value );
		if( delta > minDelta ) continue;
		minDelta = delta;
		minDeltaIndex = i;
	}

	DebugAssert( minDeltaIndex >= 0 && minDeltaIndex < m_comboBox->count() );
	if( minDeltaIndex < 0 || minDeltaIndex >= m_comboBox->count() ) return;

	m_internalIndexChange = true;
	m_comboBox->setCurrentIndex( minDeltaIndex );
	m_internalIndexChange = false;
}

void ZoomPanel::Selected(int index)
{
	UNUSED( index );
	emit ScaleSelected( CurrentScale() );
}

void ZoomPanel::IndexChanged(int index)
{
	emit SetZoomInEnabled( index < m_zoomLevels.count() - 1 );
	emit SetZoomOutEnabled( index > 0 );

	if( m_internalIndexChange ) return;
	emit ScaleSelected( CurrentScale() );
}

QList<int> ZoomPanel::CreateZoomLevels()
{
	QList<int> zoomLevels;

	zoomLevels.append( 50 );
	zoomLevels.append( 75 );
	zoomLevels.append( 100 );
	zoomLevels.append( 125 );
	zoomLevels.append( 150 );
	zoomLevels.append( 200 );
	zoomLevels.append( 250 );
	zoomLevels.append( 300 );

	return zoomLevels;
}
