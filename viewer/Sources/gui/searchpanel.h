#ifndef SEARCHPANEL_H
#define SEARCHPANEL_H

#include <QWidget>

#include <common.h>

class QBoxLayout;
class QLineEdit;
class QLabel;
class QGroupBox;
class QPushButton;
class SearchResults;

class SearchPanel : public QWidget
{
	Q_OBJECT
public:
	explicit SearchPanel(QWidget *parent = 0);
	virtual ~SearchPanel();
	void Translate(void);

	void Load( int pagesCount, Request* baseRequest );
	void Clear(void);

public slots:
	void Select( int pageIndex );

signals:
	void GotoSearch( int pageIndex, WordCoordinates coordinates );

private slots:
	void Search();
	void TextChanged(QString text);
	void SlotGotoSearch( int pageIndex, WordCoordinates coordinates );
	void SearchFinished();
	void NothingWasFound();
	void SearchProgress( int received, int sent );

private:
	// Private methods
	void CreateGui(void);
	void ConnectSlots(void);

	// Gui items
	QLabel*			m_label;
	QLineEdit*		m_textEdit;
	QPushButton*	m_searchButton;
	QPushButton*	m_clearButton;
	QLabel*			m_status;
	QGroupBox*		m_groupBox;
	SearchResults*	m_searchResults;
	QBoxLayout*		m_verticalLayout;

	Request*	m_baseRequest;
	int			m_pagesCount;
};

#endif // SEARCHPANEL_H
