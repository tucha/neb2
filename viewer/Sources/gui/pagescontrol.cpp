#include "pagescontrol.h"

#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QIntValidator>

#include <defines.h>

PagesControl::PagesControl(QWidget *parent) :
	QWidget(parent),
	m_layout( new QHBoxLayout( this ) ),
	m_pageIndex( new QLineEdit( this ) ),
	m_pagesCount( new QLineEdit( this ) ),
	m_labelHeader( new QLabel( this ) ),
	m_labelSeparator( new QLabel( this ) ),
	m_validator( new QIntValidator( this ) )
{
	m_layout->addWidget( m_labelHeader );
	m_layout->addWidget( m_pageIndex );
	m_layout->addWidget( m_labelSeparator );
	m_layout->addWidget( m_pagesCount );

	m_pagesCount->setEnabled( false );
	m_pageIndex->setValidator( m_validator );
	m_validator->setBottom( 1 );

	QLabel sizer( NULL );
	sizer.setText( "00000" );
	m_pagesCount->setMaximumWidth( sizer.sizeHint().width() );
	m_pageIndex->setMaximumWidth( sizer.sizeHint().width() );

	this->setLayout( m_layout );
	this->setContentsMargins( 0,0,0,0 );
	this->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );

	connect(m_pageIndex,SIGNAL(returnPressed()),this,SLOT(ReturnPressed()));

	Translate();
}

void PagesControl::Translate()
{
	m_labelHeader->setText( tr( "Page" ) );
	m_labelSeparator->setText( tr( "/" ) );
}

void PagesControl::Clear()
{
	m_pagesCount->setText( "" );
	m_pageIndex->setText( "" );
}

void PagesControl::SetPagesCount(int pagesCount)
{
	m_pagesCount->setText( QString::number( pagesCount ) );
	m_validator->setTop( pagesCount );
}

void PagesControl::SetPageIndex(int pageIndex)
{
	m_pageIndex->setText( QString::number( pageIndex + 1 ) );
}

void PagesControl::ReturnPressed()
{
	bool ok;
	int pageIndex = m_pageIndex->text().toInt( &ok );

	DebugAssert( ok );
	if( ok == false ) return;

	emit GotoPage( pageIndex );
}
