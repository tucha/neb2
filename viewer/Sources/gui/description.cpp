#include "description.h"
#include "record.h"

#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QDialogButtonBox>

#include <defines.h>

Description::Description(QWidget *parent) :
	QDialog(parent),

	m_layout( new QFormLayout() ),

	m_recordTitle( new Record( this ) ),
	m_recordAuthor( new Record( this ) ),
	m_recordPlace( new Record( this ) ),
	m_recordPublisher( new Record( this ) ),
	m_recordYear( new Record( this ) ),
	m_recordDescription( new Record( this ) ),
	m_recordISBN( new Record( this ) ),
	m_recordUDK( new Record( this ) ),
	m_recordBBK( new Record( this ) ),
	m_recordSign( new Record( this ) ),
	m_recordAnnotation( new Record( this ) )
{
	CreateGui();
	Translate();

	setMinimumWidth( 700 );
	setMinimumHeight( 300 );
	setModal( true );
//	setWindowFlags( Qt::Window | Qt::WindowCloseButtonHint );
	setPalette( QPalette( Qt::white ) );
}

Description::~Description()
{
	Clear();
}

void Description::Load(Request* baseRequest)
{
	m_recordTitle->Load(		baseRequest, "245", "a", PRIORITY_HIGHT );
	m_recordAuthor->Load(		baseRequest, "100", "a" );
	m_recordPlace->Load(		baseRequest, "260", "a" );
	m_recordPublisher->Load(	baseRequest, "260", "b" );
	m_recordYear->Load(			baseRequest, "260", "c" );
	m_recordDescription->Load(	baseRequest, "300", "a" );
	m_recordISBN->Load(			baseRequest, "534", "z" );
//	m_recordUDK->Load(			baseRequest, "", "" );
	m_recordBBK->Load(			baseRequest, "084", "a" );
//	m_recordSign->Load(			baseRequest, "", "" );
//	m_recordAnnotation->Load(	baseRequest, "", "" );
}

void Description::Clear()
{
	m_recordTitle->Clear();
	m_recordAuthor->Clear();
	m_recordPlace->Clear();
	m_recordPublisher->Clear();
	m_recordYear->Clear();
	m_recordDescription->Clear();
	m_recordISBN->Clear();
	m_recordUDK->Clear();
	m_recordBBK->Clear();
	m_recordSign->Clear();
	m_recordAnnotation->Clear();
}

#define AddRow( T ) m_layout->addRow( T->Label(), T->Value() )

void Description::CreateGui()
{
	m_layout->setHorizontalSpacing( 50 );
	m_layout->setVerticalSpacing( 0 );

#ifdef OS_MACOSX
	m_layout->setFieldGrowthPolicy( QFormLayout::AllNonFixedFieldsGrow );
#endif

	AddRow( m_recordYear );
	AddRow( m_recordAuthor );
	AddRow( m_recordTitle );
	AddRow( m_recordPublisher );
	AddRow( m_recordPlace );
	AddRow( m_recordDescription );
	AddRow( m_recordISBN );
	AddRow( m_recordUDK );
	AddRow( m_recordBBK );
	AddRow( m_recordSign );
	AddRow( m_recordAnnotation );

	QFont font = m_recordYear->Value()->font();
	font.setUnderline( true );
	m_recordYear->Value()->setFont( font );

	QVBoxLayout* mainLayout = new QVBoxLayout;
	mainLayout->addLayout( m_layout );

	QDialogButtonBox* buttonBox = new QDialogButtonBox( QDialogButtonBox::Close );
	buttonBox->button( QDialogButtonBox::Close )->setText( tr( "Close" ) );
	connect( buttonBox, SIGNAL( rejected() ), this, SLOT( reject() ) );
	mainLayout->addWidget( buttonBox );

	this->setLayout( mainLayout );
}

void Description::Translate()
{
	m_recordTitle->SetLabel( tr( "Title" ) );
	m_recordAuthor->SetLabel( tr( "Author" ) );
	m_recordPlace->SetLabel( tr( "Place" ) );
	m_recordPublisher->SetLabel( tr( "Publisher" ) );
	m_recordYear->SetLabel( tr( "Year" ) );
	m_recordDescription->SetLabel( tr( "Description" ) );
	m_recordISBN->SetLabel( tr( "ISBN" ) );
	m_recordUDK->SetLabel( tr( "UDK" ) );
	m_recordBBK->SetLabel( tr( "BBK" ) );
	m_recordSign->SetLabel( tr( "Sign" ) );
	m_recordAnnotation->SetLabel( tr( "Annotation" ) );

	this->setWindowTitle( tr( "DocumentDescription" ) );
}
