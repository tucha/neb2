<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AuthorizationDialog</name>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="107"/>
        <location filename="gui/authorizationdialog.cpp" line="116"/>
        <source>title</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="107"/>
        <source>error</source>
        <translation>Не удаётся подключиться к серверу. Проверьте подключение к Интернет и доступность сайта &lt;a href=&quot;http://online.archives.ru&quot;&gt;http://online.archives.ru&lt;/a&gt; с вашего компьютера.</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="159"/>
        <source>UserName</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="160"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="161"/>
        <source>Ok</source>
        <translation>Ок</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="162"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="164"/>
        <source>Authentication</source>
        <translation>Авторизация</translation>
    </message>
</context>
<context>
    <name>Card</name>
    <message>
        <source>DebugOutput</source>
        <translation type="vanished">Оповещения</translation>
    </message>
    <message>
        <source>DocumentDescription</source>
        <translation type="vanished">Карточка документа</translation>
    </message>
</context>
<context>
    <name>CardTabs</name>
    <message>
        <source>Favorite</source>
        <translation type="vanished">Избранное</translation>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">Последние</translation>
    </message>
</context>
<context>
    <name>CommentWindow</name>
    <message>
        <location filename="gui/commentwindow.cpp" line="84"/>
        <source>AddComment</source>
        <oldsource>AddBookmark</oldsource>
        <translation>Добавить комментарий</translation>
    </message>
    <message>
        <source>SaveBookmark</source>
        <translation type="vanished">Сохранить закладку</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="84"/>
        <source>SaveComment</source>
        <translation>Сохранить комментарий</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="86"/>
        <source>DeleteComment</source>
        <oldsource>DeleteBookmark</oldsource>
        <translation>Удалить комментарий</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="87"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="88"/>
        <source>Comment</source>
        <translation>Введите новый комментарий</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="89"/>
        <source>CommentWindow</source>
        <translation>Комментарий</translation>
    </message>
</context>
<context>
    <name>Description</name>
    <message>
        <location filename="gui/description.cpp" line="106"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="115"/>
        <source>Title</source>
        <translation>Заглавие:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="116"/>
        <source>Author</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="117"/>
        <source>Place</source>
        <translation>Место издания:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="118"/>
        <source>Publisher</source>
        <translation>Издательство:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="119"/>
        <source>Year</source>
        <translation>Год издания:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="120"/>
        <source>Description</source>
        <translation>Физическое описание:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="121"/>
        <source>ISBN</source>
        <translation>ISBN:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="122"/>
        <source>UDK</source>
        <translation>УДК:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="123"/>
        <source>BBK</source>
        <translation>ББК:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="124"/>
        <source>Sign</source>
        <translation>Авторский знак:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="125"/>
        <source>Annotation</source>
        <translation>Аннотация:</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Открыть</translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="vanished">Скачать</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="127"/>
        <source>DocumentDescription</source>
        <translation>Библиографическое описание</translation>
    </message>
</context>
<context>
    <name>Display</name>
    <message>
        <source>Page %1/%2
Forbidden</source>
        <translation type="vanished">Страница %1/%2
Доступ запрещён</translation>
    </message>
    <message>
        <location filename="gui/display.cpp" line="71"/>
        <source>AccessForbidden</source>
        <translation>Доступ запрещён</translation>
    </message>
</context>
<context>
    <name>DownloadWindow</name>
    <message>
        <location filename="gui/downloadwindow.cpp" line="57"/>
        <source>Save document</source>
        <translation>Сохранить документ</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="57"/>
        <source>PDF Files (*.pdf)</source>
        <translation>PDF файл (*.pdf)</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="55"/>
        <source>Downloading...</source>
        <translation>Скачивание...</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="171"/>
        <source>Cancel</source>
        <translation>Прервать</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="172"/>
        <source>Saving Document</source>
        <translation>Сохранение документа</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="195"/>
        <source>Downloading page %1 ( %2 of %3 ) ...</source>
        <translation>Скачивание страницы %1 (%2 из %3) ...</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="221"/>
        <source>AccessError</source>
        <translation>Ошибка доступа</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="222"/>
        <source>Pages where not saved: %1</source>
        <translation>У вас нет прав на электронное копирование следующих страниц: %1</translation>
    </message>
    <message>
        <source>Downloading page %1 of %2 ...</source>
        <oldsource>Downloading page %1...</oldsource>
        <translation type="vanished">Скачивание страницы %1 из %2...</translation>
    </message>
</context>
<context>
    <name>Favorite</name>
    <message>
        <source>Favorite</source>
        <translation type="vanished">Избранное</translation>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">Последние</translation>
    </message>
</context>
<context>
    <name>FavoriteAndHistory</name>
    <message>
        <source>Favorite</source>
        <translation type="vanished">Избранное</translation>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">Последние</translation>
    </message>
</context>
<context>
    <name>FavoritesRecord</name>
    <message>
        <source>Title: %1</source>
        <translation type="vanished">%1</translation>
    </message>
    <message>
        <source>Author: %1</source>
        <translation type="vanished">%1</translation>
    </message>
</context>
<context>
    <name>FavoritesView</name>
    <message>
        <location filename="gui/FavoritesView.cpp" line="52"/>
        <location filename="gui/FavoritesView.cpp" line="111"/>
        <source>Loading...</source>
        <translation type="unfinished">Загрузка...</translation>
    </message>
    <message>
        <location filename="gui/FavoritesView.cpp" line="162"/>
        <source>Author: %1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="gui/FavoritesView.cpp" line="164"/>
        <source>Title: %1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="gui/history.cpp" line="36"/>
        <source>delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>HistoryRecord</name>
    <message>
        <location filename="gui/historyrecord.cpp" line="130"/>
        <source>Page %1
Forbidden</source>
        <translation>Страница %1
Доступ запрещён</translation>
    </message>
    <message>
        <location filename="gui/historyrecord.cpp" line="169"/>
        <source>DownloadingError</source>
        <translation>Ошибка при скачивании</translation>
    </message>
    <message>
        <location filename="gui/historyrecord.cpp" line="180"/>
        <source>Title: %1</source>
        <translation>%1</translation>
    </message>
    <message>
        <source>Remove from history</source>
        <translation type="vanished">Удалить из списка последних документов</translation>
    </message>
</context>
<context>
    <name>Logic</name>
    <message>
        <source>NotAllowedIpTitle</source>
        <translation type="vanished">ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <source>NotAllowedIpMessage</source>
        <translation type="vanished">Уважаемый читатель! Вы приступаете к чтению издания, защищенного авторским правом. Доступ к документу предоставляется в соответствии со статьей 1274 IV Части Гражданского Кодекса Российский Федерации.</translation>
    </message>
    <message>
        <source>NotAllowedIpButtonText</source>
        <translation type="vanished">Продолжить чтение</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="116"/>
        <source>ConfigPresentedTitle</source>
        <translation>ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="117"/>
        <source>ConfigPresentedMessage</source>
        <translation>Внимание! Используется файл переопределения ответов от сервера.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="274"/>
        <source>DownloadingError</source>
        <translation>Документ не найден</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="275"/>
        <source>Downloading %1 was unsuccessful.</source>
        <translation>Не удалось открыть документ %1.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="511"/>
        <location filename="logic.cpp" line="520"/>
        <location filename="logic.cpp" line="529"/>
        <location filename="logic.cpp" line="540"/>
        <location filename="logic.cpp" line="551"/>
        <location filename="logic.cpp" line="565"/>
        <location filename="logic.cpp" line="717"/>
        <source>title</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="512"/>
        <location filename="logic.cpp" line="718"/>
        <source>message</source>
        <translation>Программа ИС ЕЭЧЗ ФА не может выполняться одновременно с приложением %1 (%2).</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="521"/>
        <location filename="logic.cpp" line="530"/>
        <location filename="logic.cpp" line="541"/>
        <location filename="logic.cpp" line="552"/>
        <source>loadavg_message</source>
        <translation>Обнаружен неподдерживаемый дистрибутив Linux. Программа ИС ЕЭЧЗ ФА будет завершена.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="566"/>
        <source>message %1 %2.</source>
        <translation>Программа ИС ЕЭЧЗ ФА не может выполняться одновременно с приложением %1 (%2).</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="590"/>
        <source>UserTokenExpired</source>
        <translation>Уважаемый пользователь! Сеанс Вашей работы в системе просмотра документов истёк или был прерван. Введите пароль для продолжения.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="612"/>
        <source>SettingsErrorTitle</source>
        <translation>ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="613"/>
        <source>SettingsErrorMessage</source>
        <translation>Получены некорректные настройки приложения. Повторите попытку позднее.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="621"/>
        <source>NeedUpdateTitle</source>
        <translation>ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="622"/>
        <source>NeedUpdateMessage</source>
        <translation>Уважаемый пользователь! Вам необходимо скачать новую версию программы.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="635"/>
        <source>SettingsRequestTimeoutTitle</source>
        <translation>ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="636"/>
        <source>SettingsRequestTimeoutMessage</source>
        <translation>Не удаётся подключиться к серверу. Проверьте подключение к Интернет и доступность сайта &lt;a href=&quot;http://online.archives.ru&quot;&gt;http://online.archives.ru&lt;/a&gt; с вашего компьютера.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="862"/>
        <source>Screenshot application for GNOME</source>
        <translation>стандартное приложение GNOME для получения снимков экрана</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="870"/>
        <source>KDE screen capture tool</source>
        <translation>стандартное приложение KDE для получения снимков экрана</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>ToolBar</source>
        <translation type="vanished">Панель инструментов</translation>
    </message>
    <message>
        <source>SearchPanel</source>
        <translation type="vanished">Поиск</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="964"/>
        <source>Thumbnails</source>
        <translation>Содержание</translation>
    </message>
    <message>
        <source>Open document</source>
        <translation type="vanished">Открыть документ</translation>
    </message>
    <message>
        <source>DocumentId</source>
        <translation type="vanished">Код документа</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="414"/>
        <source>PrintDocument</source>
        <translation>Печать документа</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="415"/>
        <source>ChoosePagesToBePrinted</source>
        <translation>Укажите страницы для печати через запятую (например 1,3-5,7,9)</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="416"/>
        <source>Print</source>
        <translation>Распечатать</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="430"/>
        <source>SaveDocument</source>
        <translation>Копирование документа</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="431"/>
        <source>ChoosePagesToBeSaved</source>
        <translation>Укажите страницы для копирования через запятую (например 1,3-5,7,9)</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="432"/>
        <source>Save</source>
        <translation>Скопировать</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="441"/>
        <source>Buy</source>
        <translation>Заказ издания</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="441"/>
        <source>Service is unavailable</source>
        <translation>Данный сервис не доступен</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1115"/>
        <source>PageIsUnavaliableTitle</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1116"/>
        <source>PageIsUnavailableMessage</source>
        <translation>Запрашиваемая страница недоступна</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1152"/>
        <source>Page %1/%2
Forbidden</source>
        <translation>Страница %1/%2
Доступ запрещён</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1330"/>
        <source>RemoveBookmark</source>
        <translation>Удалить закладку</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1330"/>
        <source>AddBookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1344"/>
        <source>EditComment</source>
        <translation>Редактировать комментарий</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1344"/>
        <source>AddComment</source>
        <translation>Добавить комментарий</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="542"/>
        <source>openTitle</source>
        <translation>Открыть документ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="543"/>
        <source>openLabel</source>
        <translation>Введите ссылку на документ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="545"/>
        <source>openCancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="555"/>
        <source>documentWrongTitle</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="555"/>
        <source>documentWrongMessage</source>
        <translation>Некорректная ссылка на документ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="666"/>
        <source>Autoscroll</source>
        <translation>Автолистание</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="967"/>
        <source>Bookmarks</source>
        <translation>Закладки</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="968"/>
        <source>Comments</source>
        <translation>Комментарии</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="969"/>
        <source>Favorite</source>
        <translation>Избранное</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="970"/>
        <source>History</source>
        <translation>Недавно открытые</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="972"/>
        <source>zoomIn</source>
        <translation>Увеличить масштаб</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="973"/>
        <source>zoomOut</source>
        <translation>Уменьшить масштаб</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1033"/>
        <source>MainWindowTitleLimited%1</source>
        <translation>ИС ЕЭЧЗ ФА. Тестовая сборка. Время использования - до %1</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1035"/>
        <source>MainWindowTitle</source>
        <translation>ИС ЕЭЧЗ ФА %1</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1102"/>
        <source>AskUserToContinueTitle</source>
        <translation>ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1103"/>
        <source>AskUserToContinueMessage</source>
        <translation>Уважаемый читатель! Документ находится в ограниченном доступе. Доступ к документу предоставляется в соответствии с Частью четвертой Гражданского Кодекса Российский Федерации. В полном объеме текст документа доступен в Читальных залах библиотек-участников НЭБ.
Подтвердите, что Вы находитесь в помещении библиотеки.</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1105"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1106"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1110"/>
        <source>PendingRequestTitle</source>
        <translation>ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1111"/>
        <source>PendingRequestMessage</source>
        <translation>В настоящий момент ожидается ответ от сервера. Повторите попытку позднее.</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1113"/>
        <location filename="gui/mainwindow.cpp" line="1118"/>
        <source>Ok</source>
        <translation>Продолжить</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1096"/>
        <source>NotAllowedIpTitle</source>
        <translation>ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1337"/>
        <source>Removefavorites</source>
        <translation>Убрать издание из избранного</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1337"/>
        <source>AddFavorites</source>
        <translation>Добавить издание в избранное</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1097"/>
        <source>NotAllowedIpMessage</source>
        <translation>Уважаемый читатель! Вы приступаете к чтению издания, защищенного авторским правом. Доступ к документу предоставляется в соответствии с Частью четвертой Гражданского Кодекса Российский Федерации.</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1098"/>
        <source>NotAllowedIpButtonText</source>
        <translation>Продолжить чтение</translation>
    </message>
    <message>
        <source>settings</source>
        <translation type="vanished">Настройки</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="974"/>
        <source>ror</source>
        <translation>Повернуть страницу по часовой стрелке</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="975"/>
        <source>rol</source>
        <translation>Повернуть страницу против часовой стрелки</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="976"/>
        <source>favorite</source>
        <translation>Добавить издание в избранное</translation>
    </message>
    <message>
        <source>buy</source>
        <translation type="vanished">Заказ издания</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="978"/>
        <source>fullScreen</source>
        <translation>Полноэкранный режим</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="980"/>
        <source>copy</source>
        <translation>Скопировать</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="982"/>
        <source>scaleWidth</source>
        <translation>Масштаб по ширине страницы</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="983"/>
        <source>scaleFull</source>
        <translation>Страница целиком</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="984"/>
        <source>nextPage</source>
        <translation>Следующая страница</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="985"/>
        <source>firstPage</source>
        <translation>Первая страница</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="986"/>
        <source>prevPage</source>
        <translation>Предыдущая страница</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="987"/>
        <source>lastPage</source>
        <translation>Последняя страница</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="988"/>
        <source>bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="989"/>
        <source>comment</source>
        <translation>Добавить комментарий</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="991"/>
        <source>print</source>
        <translation>Распечатать</translation>
    </message>
    <message>
        <source>revertColors</source>
        <translation type="vanished">Инвертировать цвета</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="993"/>
        <source>invert</source>
        <translation>Инвертировать цвета</translation>
    </message>
    <message>
        <source>ShowCard</source>
        <translation type="vanished">Библиографическое описание</translation>
    </message>
    <message>
        <source>open</source>
        <translation type="vanished">Открыть документ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1021"/>
        <source>ToolBarScaling</source>
        <translation>Масштабирование</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1023"/>
        <source>ToolBarDocument</source>
        <translation>Операции с документами</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1025"/>
        <source>ToolBarNavigation</source>
        <translation>Навигация по документу</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1026"/>
        <source>ToolBarView</source>
        <translation>Вид документа</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1027"/>
        <source>ToolBarPrivate</source>
        <translation>Личный кабинет</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1028"/>
        <source>ToolBarOthers</source>
        <translation>Другие инструменты</translation>
    </message>
    <message>
        <source>MainWindowTitile</source>
        <translation type="obsolete">ИС ЕЭЧЗ ФА. Тестовая сборка. Время использования - до %1</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1030"/>
        <source>DebugOutput</source>
        <translation>Оповещения</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">Увеличить масштаб</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">Уменьшить масштаб</translation>
    </message>
</context>
<context>
    <name>PageSetWindow</name>
    <message>
        <source>SaveDocument</source>
        <translation type="vanished">Копирование документа</translation>
    </message>
    <message>
        <source>ChoosePagesToBeSaved</source>
        <translation type="vanished">Укажите страницы для копирования</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">Скопировать</translation>
    </message>
    <message>
        <location filename="gui/pagesetwindow.cpp" line="74"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
</context>
<context>
    <name>PagesControl</name>
    <message>
        <location filename="gui/pagescontrol.cpp" line="44"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="gui/pagescontrol.cpp" line="45"/>
        <source>/</source>
        <translation>/</translation>
    </message>
</context>
<context>
    <name>PrintingWindow</name>
    <message>
        <location filename="gui/printingwindow.cpp" line="58"/>
        <source>Downloading...</source>
        <translation>Скачивание...</translation>
    </message>
    <message>
        <source>Save document</source>
        <translation type="obsolete">Сохранить документ</translation>
    </message>
    <message>
        <source>PDF Files (*.pdf)</source>
        <translation type="vanished">PDF файл (*.pdf)</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="67"/>
        <source>PrintDocument</source>
        <translation>Печать документа</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="158"/>
        <source>Cancel</source>
        <translation>Прервать</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="159"/>
        <source>Printing Document</source>
        <oldsource>Saving Document</oldsource>
        <translation>Печать документа</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="182"/>
        <source>Downloading page %1 ( %2 of %3 ) ...</source>
        <translation>Скачивание страницы %1 (%2 из %3) ...</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="249"/>
        <source>AccessError</source>
        <translation>Ошибка доступа</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="250"/>
        <source>Pages where not printed: %1</source>
        <translation>У Вас нет прав на печать следующих страниц: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="protection/printscreenprotection.cpp" line="53"/>
        <source>Print Screen option is disabled.</source>
        <translation>Создание снимков экрана невозможно.</translation>
    </message>
    <message>
        <location filename="protection/printscreenprotection.cpp" line="54"/>
        <source>To make a snapshot, please close the protected document.</source>
        <translation>Закройте защищённый документ и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="miscellaneous/parameters.cpp" line="164"/>
        <location filename="miscellaneous/parameters.cpp" line="168"/>
        <source>Wrong parameter %1</source>
        <translation>Некорректная ссылка на документ: %1</translation>
    </message>
    <message>
        <location filename="miscellaneous/parameters.cpp" line="167"/>
        <source>ErrorTitle</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../common/authorizationresult.cpp" line="29"/>
        <source>Unknown server reply</source>
        <translation>Некорректный ответ от сервера</translation>
    </message>
    <message>
        <source>Only KDE, GNOME and Unity desktop environments are supported.</source>
        <translation type="vanished">Поддерживаются только следующие окружения рабочего стола: KDE, GNOME, Unity.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>An attempt to use a virtual PC has been detected. Please close all PC emulators and try again.</source>
        <translation>Была обнаружена попытка использования системного эмулятора. Пожалуйста, закройте все эмулирующие программы и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="94"/>
        <source>TimeLimitWarningTitle</source>
        <translation>Не удаётся запустить приложение</translation>
    </message>
    <message>
        <location filename="main.cpp" line="94"/>
        <source>TimeLimitWarningLabel</source>
        <translation>Срок действия приложения истёк</translation>
    </message>
    <message>
        <source>Print Screen option is disabled.
Please close GNOME ScreenShot application first.</source>
        <translation type="vanished">Создание снимков экрана невозможно.</translation>
    </message>
</context>
<context>
    <name>SearchPanel</name>
    <message>
        <location filename="gui/searchpanel.cpp" line="29"/>
        <source>Text for search:</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="31"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="32"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="49"/>
        <source>Search results:</source>
        <translation>Результаты поиска:</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="66"/>
        <source>UncorrectSearchTitle</source>
        <translation>ИС ЕЭЧЗ ФА</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="67"/>
        <source>UncorrectSearchMessage</source>
        <translation>Поисковая строка может содержать только буквы, цифры и пробелы</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="74"/>
        <source>Searching...</source>
        <translation>Поиск...</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="91"/>
        <source>Searching was finished</source>
        <translation>Поиск завершён</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="96"/>
        <source>Nothing was found</source>
        <translation>Ничего не найдено</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="101"/>
        <source>Searching %1 percents where finished</source>
        <translation>Поиск... %1% завершено</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="gui/searchresult.cpp" line="144"/>
        <source>Unexpected service respose</source>
        <translation>Некорректный ответ от сервиса выдачи документов</translation>
    </message>
</context>
<context>
    <name>Thumbnails</name>
    <message>
        <location filename="gui/thumbnails.cpp" line="48"/>
        <source>loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="gui/thumbnails.cpp" line="67"/>
        <source>empty</source>
        <translation>Пусто</translation>
    </message>
    <message>
        <location filename="gui/thumbnails.cpp" line="175"/>
        <source>loading %1%...</source>
        <oldsource>loading %1/%...</oldsource>
        <translation>Загрузка... %1% завершено</translation>
    </message>
    <message>
        <location filename="gui/thumbnails.cpp" line="179"/>
        <source>finished</source>
        <translation>Загрузка завершена</translation>
    </message>
</context>
<context>
    <name>UserPasswordDialog</name>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="65"/>
        <source>UserName</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="66"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="67"/>
        <source>Ok</source>
        <translation>Ок</translation>
    </message>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="68"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="70"/>
        <source>Authentication</source>
        <translation>Аутентификация</translation>
    </message>
</context>
<context>
    <name>ZoomPanel</name>
    <message>
        <location filename="gui/zoompanel.cpp" line="36"/>
        <source>Scale</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location filename="gui/zoompanel.cpp" line="44"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
</context>
</TS>
