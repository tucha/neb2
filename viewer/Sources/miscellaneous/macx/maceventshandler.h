#ifndef MACEVENTSHANDER_H
#define MACEVENTSHANDER_H

#include "defines.h"

#include <QObject>
#include <QEvent>

#include <miscellaneous/parameters.h>

#ifndef OS_MACOSX
#error "This file can be compiled only under Mac OS X"
#endif

class BaseEvent : public QEvent
{
public:
	explicit BaseEvent( QEvent::Type& eType );
	~BaseEvent();

	static QEvent::Type getEventType( QEvent::Type& eType );
};

class ViewerDryRunEvent : public BaseEvent
{
	Parameters m_params;

public:
	explicit ViewerDryRunEvent( const Parameters& params );
	~ViewerDryRunEvent();

	Parameters GetCmdLineParams() const;

public:
	static QEvent::Type eventType;
};

//-----------------------------------------------------------------------------

class EventFilterInstaller
{
	QObject* m_eventSource;
	QObject* m_eventFilter;

public:
	EventFilterInstaller( QObject* eventSource, QObject* eventFilter );
	~EventFilterInstaller();
};

#endif // MACEVENTSHANDER_H
