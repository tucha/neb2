#include "scaler.h"

#include <QScrollBar>
#include <QTransform>

#define RESTRICTION_MIN 400
#define RESTRICTION_MAX 2000

Scaler::Scaler(QObject* parent) :
	QObject( parent ),

	m_pageSize(),
	m_displaySize(),

	m_scale( 100 ),
	m_scaleWidth( false ),
	m_scaleFull( false ),
	m_angle( 0 )
{
}

void Scaler::SetDisplaySize(QSize displaySize)
{
	m_displaySize = displaySize;
	emit ImplicitScaleChanged( ImplicitScale() );
}

void Scaler::SetPageSize(QSize pageSize)
{
	m_pageSize = pageSize;
	emit ImplicitScaleChanged( ImplicitScale() );
}

void Scaler::SetExplicitScale(int scale)
{
	m_scale = scale;
	m_scaleWidth = false;
	m_scaleFull = false;
	emit ImplicitScaleChanged( ImplicitScale() );
}

void Scaler::SetScaleWidth(bool value)
{
	m_scaleWidth = value;
	emit ImplicitScaleChanged( ImplicitScale() );
}

void Scaler::SetScaleFull(bool value)
{
	m_scaleFull = value;
	emit ImplicitScaleChanged( ImplicitScale() );
}

void Scaler::Rotate(int angle)
{
	m_angle += angle;
	emit ImplicitScaleChanged( ImplicitScale() );
}

void Scaler::ResetRotation()
{
	m_angle = 0;
	emit ImplicitScaleChanged( ImplicitScale() );
}

QPixmap Scaler::Transform(QPixmap pixmap)const
{
	DebugAssert( IsValid() );
	if( IsValid() == false ) return pixmap;

	if( m_scaleWidth ) return TransformWidth( pixmap );
	if( m_scaleFull ) return TransformFull( pixmap );
	return TransformScaled( pixmap );
}

int Scaler::Restriction() const
{
	DebugAssert( IsValid() );
	if( IsValid() == false ) return RESTRICTION_MIN;

	int result = 0;
	if( m_scaleWidth ) result = RestrictionWidth();
	else if( m_scaleFull ) result = RestrictionFull();
	else result = RestrictionScaled();

	if( result < RESTRICTION_MIN ) result = RESTRICTION_MIN;
	if( result > RESTRICTION_MAX ) result = RESTRICTION_MAX;
	return result;
}

int Scaler::ExplicitScale() const
{
	return m_scale;
}

bool Scaler::ScaleWidth() const
{
	return m_scaleWidth;
}

bool Scaler::ScaleFull() const
{
	return m_scaleFull;
}

int Scaler::Angle() const
{
	return m_angle;
}

int Scaler::ImplicitScale() const
{
	if( IsValid() == false ) return 100;

	if( m_scaleWidth ) return 100 * ImplicitScaleWidth();
	if( m_scaleFull ) return 100 * ImplicitScaleFull();
	return ExplicitScale();

}

QPixmap Scaler::TransformScaled(QPixmap pixmap) const
{
	qreal scaleNeeded = ( qreal )m_scale / 100;
	qreal scaleHave = ( qreal )pixmap.width() / m_pageSize.width();

	qreal scale = scaleNeeded / scaleHave;

	return pixmap.transformed( QTransform::fromScale( scale, scale ).rotate( m_angle ) );
}

int Scaler::RestrictionScaled() const
{
	qreal scale = ( qreal )m_scale / 100;
	return m_pageSize.width() * scale;
}

QPixmap Scaler::TransformFull(QPixmap pixmap) const
{
	return pixmap.scaledToWidth( RestrictionFull() ).transformed( QTransform().rotate( m_angle ) );
}

int Scaler::RestrictionFull() const
{
	return m_pageSize.width() * ImplicitScaleFull() - 1;
}

qreal Scaler::ImplicitScaleFull() const
{
	if( IsRotated() )
	{
		qreal proportionDisplay = ( qreal )m_displaySize.width() / m_displaySize.height();
		qreal proportionPage = ( qreal )m_pageSize.height() / m_pageSize.width();
		if( proportionDisplay > proportionPage )
		{
			qreal scale = ( qreal )m_displaySize.height() / m_pageSize.width();
			return scale;
		}
		else
		{
			qreal scale = ( qreal )m_displaySize.width() / m_pageSize.height();
			return scale;
		}
	}
	else
	{
		qreal proportionDisplay = ( qreal )m_displaySize.width() / m_displaySize.height();
		qreal proportionPage = ( qreal )m_pageSize.width() / m_pageSize.height();
		if( proportionDisplay > proportionPage )
		{
			qreal scale = ( qreal )m_displaySize.height() / m_pageSize.height();
			return scale;
		}
		else
		{
			qreal scale = ( qreal )m_displaySize.width() / m_pageSize.width();
			return scale;
		}
	}
}

QPixmap Scaler::TransformWidth(QPixmap pixmap) const
{
	return pixmap.scaledToWidth( RestrictionWidth() ).transformed( QTransform().rotate( m_angle ) );
}

int Scaler::RestrictionWidth() const
{
	if( IsRotated() )
	{
		int result = m_displaySize.width() - QScrollBar( Qt::Vertical ).sizeHint().width();
		qreal multiplier = ( qreal )m_pageSize.width() / m_pageSize.height();
		result *= multiplier;
		return result;

	}
	else
	{
		int result = m_displaySize.width() - QScrollBar( Qt::Vertical ).sizeHint().width();
		return result;
	}
}

qreal Scaler::ImplicitScaleWidth() const
{
	qreal scale = ( qreal )RestrictionWidth() / m_pageSize.width();
	return scale;
}

bool Scaler::IsRotated() const
{
	DebugAssert( IsValid() );
	if( IsValid() == false ) return false;
	return m_angle % 180 != 0;
}

bool Scaler::IsValid() const
{
	if( m_angle % 90 != 0 ) return false;
	if( m_pageSize.isValid() == false ) return false;
	if( m_displaySize.isValid() == false ) return false;
	if( m_scaleFull && m_scaleWidth ) return false;
	return true;
}
