#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QStringList>
#include <QHash>

#include "documentstate.h"

#include <document.h>
#include <defines.h>

class QSettings;
class User;

class Config : public QObject
{
	Q_OBJECT
private:
	DISABLE_COPY( Config )

public:
	explicit Config(QObject *parent = 0);
	virtual ~Config();

	QString ClientId(void)const;
	Document LastDocument(void)const;

	void AddToHistory( Document document );
	void RemoveFromHistory( Document document );
	DocumentList GetHistory(void)const;

	void SetState( Document document, DocumentState state );
	DocumentState GetState( Document document )const;

	void Load(const User& user );
	bool IsLoaded(void)const;

	QString LastEmail(void)const;
	void SetLastEmail( QString value );

	QByteArray GetWindowState() const;
	void SetWindowState( QByteArray value );

	static QString Application();
	static QString Organization();

public slots:
	void Save();

signals:
	void WasChanged();

private:	
	void SaveHistory(void);
	void LoadHistory(void);
	void SaveStates(void);
	void LoadStates(void);

	QSettings* m_settings;

	QString m_clientId;
	DocumentList m_history;
	QHash<Document, DocumentState> m_states;
	bool m_isLoaded;
	QString m_email;
};

#endif // CONFIG_H
