#include "painter.h"

QImage Painter::Highlight(QImage image, WordCoordinates coordinates)
{
	QImage result = image;
	QPainter painter( &result );
	painter.setBrush( QBrush( QColor( 0, 255, 0, 150 ) ) );
	painter.setPen( Qt::NoPen );

	foreach( QString text, coordinates.keys() )
	{
		foreach( Word word, coordinates[ text ] )
		{
			QRectF place = Place( result.size(), word.Rectangle() );
			painter.drawRect( place );
		}
	}

	return result;
}

QImage Painter::Invert(QImage image)
{
	QImage result = image;
	QPainter painter( &result );
	painter.setCompositionMode( QPainter::RasterOp_NotSource );
	painter.drawImage( 0, 0, image );
	return result;
}

QRectF Painter::Place(QSize size, QRectF percents)
{
	float left = size.width() * percents.left();
	float top = size.height() * ( 1 - percents.top() );
	float right = size.width() *  percents.right();
	float bottom = size.height() * ( 1 - percents.bottom() );
	QRectF result;
	result.setLeft( left );
	result.setTop( top );
	result.setRight( right );
	result.setBottom( bottom );
	return result;
}
