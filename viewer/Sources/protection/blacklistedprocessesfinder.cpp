#include "blacklistedprocessesfinder.h"
#include "blacklistedprocessesdatabase.h"

#ifdef OS_WINDOWS

#include <defines.h>

BlacklistedProcessesFinder* BlacklistedProcessesFinder::m_instance = NULL;

bool BlacklistedProcessesFinder::SearchForBlacklistedProcess( QString& processName, QString& processDescription, uint& reportNumber )
{
	if( m_processes.isEmpty() )
	{
		LoadWindowsInfo();
		LoadProcesses();
		return false;
	}

	DWORD processId = m_processes.keys().first();
	PROCESSENTRY32 pe32 = m_processes.value( processId );
	m_processes.remove( processId );

	if( m_currentProcessId == processId ) return false;

	QString processNameFromToolSnap = QString::fromUtf16( ( const ushort* )pe32.szExeFile );
	BlacklistedProcessDescription* processInfo;
	if( ProcessIsBlacklisted( processId, processNameFromToolSnap, &processInfo ) )
	{
		processName = processNameFromToolSnap;
		processDescription = processInfo->GetProcessDescription();
		reportNumber = processInfo->GetReportNumber();
		return true;
	}

	return false;
}

bool BlacklistedProcessesFinder::ProcessIsBlacklisted( const DWORD procId, const QString& processName, BlacklistedProcessDescription** ppProcessInfoResult )
{
	return BlacklistedProcessesDatabase::FindProcess( procId, processName, ppProcessInfoResult );
}

BOOL CALLBACK BlacklistedProcessesFinder::EnumWindowProc( HWND hWnd, LPARAM lParam )
{
	BlacklistedProcessesFinder* pFinder = ( BlacklistedProcessesFinder* )lParam;
	DWORD processId;
	if( ::GetWindowThreadProcessId( hWnd, &processId ) ) pFinder->AddWindowInfo( processId, hWnd );
	return TRUE;
}

void BlacklistedProcessesFinder::LoadWindowsInfo()
{
	m_windows.clear();
	EnumWindows( ( WNDENUMPROC )EnumWindowProc, ( LPARAM )this );
}

void BlacklistedProcessesFinder::LoadProcesses()
{
	DebugAssert( m_processes.isEmpty() );
	if( m_processes.isEmpty() == false ) return;

	HANDLE hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
	DebugAssert( hProcessSnap != INVALID_HANDLE_VALUE );
	if( hProcessSnap == INVALID_HANDLE_VALUE ) return;

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof( PROCESSENTRY32 );
	if( Process32First(  hProcessSnap, &pe32 ) )
	{
		do
		{
			DWORD processId = pe32.th32ProcessID;

			DebugAssert( m_processes.contains( processId ) == false );
			if( m_processes.contains( processId ) ) continue;

			m_processes.insert( processId, pe32 );
		}
		while( Process32Next( hProcessSnap, &pe32 ) );
	}
	CloseHandle( hProcessSnap );
}

void BlacklistedProcessesFinder::AddWindowInfo( DWORD processId, HWND hWnd )
{
	if( m_currentProcessId == processId ) return;

	if( m_windows.contains( processId ) == false ) m_windows.insert( processId, WinProcessInfo( processId ) );
	m_windows[ processId ].AddWindow( hWnd );
}

WinProcessInfo BlacklistedProcessesFinder::GetWinProcessInfo( const DWORD processId )
{
	return m_windows.value( processId, WinProcessInfo( processId ) );
}

// Initialize and free resources in database of blacklisted processes
BlacklistedProcessesFinder::BlacklistedProcessesFinder()
{
	BlacklistedProcessesDatabase::InitializeProcessesDatabase();
	m_currentProcessId = ::GetCurrentProcessId();
}

BlacklistedProcessesFinder::~BlacklistedProcessesFinder()
{
	BlacklistedProcessesDatabase::DeinitializeProcessesDatabase();
}

BlacklistedProcessesFinder* BlacklistedProcessesFinder::GetInstance()
{
	if( BlacklistedProcessesFinder::m_instance == NULL )
	{
		BlacklistedProcessesFinder::m_instance = new BlacklistedProcessesFinder();
	}
	return BlacklistedProcessesFinder::m_instance;
}

void BlacklistedProcessesFinder::FreeInstance()
{
	DeleteAndNull( m_instance );
}

#endif // OS_WINDOWS
