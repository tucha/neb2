#ifndef PRINTSCREENPROTECTION_H
#define PRINTSCREENPROTECTION_H

#ifdef OS_WINDOWS

#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <Windows.h>

class QMessageBox;

class PrintScreenProtection
{
private:
	PrintScreenProtection();

public:
	static void Install();
	static void Remove();

private:
	static LRESULT CALLBACK getMessageHookForPrintScreenProtection( int code, WPARAM wParam, LPARAM lParam );

	static HHOOK g_getMessageHook;

	static int g_altPrtScHookID;
	static int g_shiftPrtScHookID;
	static int g_ctrPrtScHookID;
	static int g_winPrtScHookID;

	static bool g_prtScHotKeyWasRegistered;
	static bool g_altPrtScHotKeyWasRegistered;
	static bool g_shiftPrtScHotKeyWasRegistered;
	static bool g_ctrPrtScHotKeyWasRegistered;
	static bool g_winPrtScHotKeyWasRegistered;

	static QMessageBox* g_messageBox;
};

#endif // OS_WINDOWS

#endif // PRINTSCREENPROTECTION_H
