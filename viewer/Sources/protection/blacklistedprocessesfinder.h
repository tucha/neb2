#ifndef BLACKLISTEDPROCESSESFINDER_H
#define BLACKLISTEDPROCESSESFINDER_H

#ifdef OS_WINDOWS

#include <Windows.h>
#include <TlHelp32.h>

#include <QtGlobal>
#include <QSet>
#include <QHash>

class BlacklistedProcessDescription;

class WinProcessInfo
{
protected:
	DWORD m_processId;
	QSet<HWND> m_windows;

public:
	WinProcessInfo(){}
	WinProcessInfo( const DWORD processId )
		: m_processId( processId )
	{
	}

	bool operator < ( const WinProcessInfo& wndProcessInfo ) const
	{
		return m_processId < wndProcessInfo.m_processId;
	}

	DWORD GetProcessId() const
	{
		return m_processId;
	}

	const QSet<HWND> GetWindows() const
	{
		return m_windows;
	}

	void AddWindow( const HWND hWnd )
	{
		m_windows.insert( hWnd );
	}
};

class BlacklistedProcessesFinder
{
private:
	BlacklistedProcessesFinder();
	~BlacklistedProcessesFinder();

	bool ProcessIsBlacklisted( const DWORD procId, const QString& processName, BlacklistedProcessDescription** ppProcessInfoResult );
	void AddWindowInfo( DWORD, HWND hWnd );
	void LoadWindowsInfo();
	void LoadProcesses();

	static BOOL CALLBACK EnumWindowProc( HWND hWnd, LPARAM lParam );
	static BlacklistedProcessesFinder* m_instance;

public:
	static BlacklistedProcessesFinder* GetInstance();
	static void FreeInstance();

	WinProcessInfo GetWinProcessInfo( const DWORD processId );
	bool SearchForBlacklistedProcess( QString& processName, QString& processDescription, uint& reportNumber );

private:
	unsigned long m_currentProcessId;
	QHash<DWORD,WinProcessInfo> m_windows;
	QHash<DWORD,PROCESSENTRY32> m_processes;
};

#endif // OS_WINDOWS

#endif // BLACKLISTEDPROCESSESFINDER_H
