#ifndef BLACKLISTEDPROCESSESDATABASE_H
#define BLACKLISTEDPROCESSESDATABASE_H

#ifdef OS_WINDOWS

#define MAX_WINDOW_CAPTION_LEN 1024

#include <Windows.h>

#include <QtGlobal>
#include <QList>
#include <QString>

/**
 * @brief Class describes entry in blacklisted processes database
 *
 * Blacklisted process is identified by process name and, optionally, by title of window that must be present in process
 */
class BlacklistWindowInfo
{
protected:
	QString m_text;
	QString m_className;
	long m_winStyle;
	bool m_styleDefined;
	bool m_searchSubstringFromBegin;

public:
	BlacklistWindowInfo() : m_winStyle( 0 ), m_styleDefined( false ), m_searchSubstringFromBegin( true )
	{}

	BlacklistWindowInfo( const QString& text, bool searchSubstringFromBegin = true )
		: m_text( text ), m_styleDefined( false ), m_searchSubstringFromBegin( searchSubstringFromBegin )
	{}

	BlacklistWindowInfo( const QString& text, const QString& className, bool searchSubstringFromBegin = true )
		: m_text( text ), m_className( className ), m_styleDefined( false ), m_searchSubstringFromBegin( searchSubstringFromBegin )
	{}

	BlacklistWindowInfo( const QString& text, const long winStyle, bool searchSubstringFromBegin = true )
		: m_text(text), m_winStyle( winStyle ), m_styleDefined( true ), m_searchSubstringFromBegin( searchSubstringFromBegin )
	{}

	BlacklistWindowInfo( const QString& text, const QString& className, const long winStyle, bool searchSubstringFromBegin = true )
		: m_text( text ), m_className( className ), m_winStyle( winStyle ), m_styleDefined( true ), m_searchSubstringFromBegin( searchSubstringFromBegin )
	{}

	BlacklistWindowInfo( const BlacklistWindowInfo& blacklistInfo )
		: m_text( blacklistInfo.m_text ), m_className( blacklistInfo.m_className ),
		m_winStyle( blacklistInfo.m_winStyle ), m_styleDefined( blacklistInfo.m_styleDefined ), m_searchSubstringFromBegin( blacklistInfo.m_searchSubstringFromBegin )
	{}

	bool CheckWindowAttributes( HWND hWnd )
	{
		if( m_className.size() )
		{
			wchar_t cBuf[ MAX_WINDOW_CAPTION_LEN ] = { 0 };
			if( ::GetClassName( hWnd, cBuf, MAX_WINDOW_CAPTION_LEN )
				&& QString::compare( QString::fromUtf16( (ushort*)cBuf ), m_className, Qt::CaseInsensitive ) != 0 )
				return false;
		}

		if( m_styleDefined )
			return( m_winStyle == ::GetWindowLong( hWnd, GWL_STYLE ) && ::GetLastError() == ERROR_SUCCESS );

		return true;
	}

	const QString& Text()
	{
		return m_text;
	}

	bool IsSearchSubstringfromBegin() const
	{
		return m_searchSubstringFromBegin;
	}
};

class BlacklistedProcessDescription
{
	QString m_processDescription;
	QString m_processName;
	QList< BlacklistWindowInfo > m_windowFullTitlesInfo;
	QList< BlacklistWindowInfo > m_windowPartTitlesInfo;
	typedef QList< BlacklistWindowInfo >::iterator winInfoIterator_type;
	uint m_reportNumber;

public:
	// Construction
	BlacklistedProcessDescription( QString processDescription, QString processName, uint reportNumber );
	BlacklistedProcessDescription( QString processDescription, uint reportNumber );

	// Process name
	QString GetProcessDescription();
	unsigned int GetReportNumber();
	bool 	ProcessNameEquals( QString processName );

	bool ContainsProcessName();
	bool ContainsWindowsTitles();

	// Window titles
	void 	AddFullTitleWindowInfo( const QString& windowTitle );
	void 	AddPartialTitleWindowInfo( const QString& windowTitlePart, bool searchSubstringFromBegin = true );
	void 	AddFullTitleWindowInfo( const QString& windowTitle, const QString& windowClassName );
	void 	AddPartialTitleWindowInfo( const QString& windowTitlePart, const QString& windowClassName, bool searchSubstringFromBegin = true );
	void 	AddFullTitleWindowInfo( const QString& windowTitle, const long winStyle );
	void 	AddPartialTitleWindowInfo( const QString& windowTitlePart, const long winStyle, bool searchSubstringFromBegin = true );
	void 	AddFullTitleWindowInfo( const QString& windowTitle, const QString& windowClassName, const long winStyle );
	void 	AddPartialTitleWindowInfo( const QString& windowTitlePart, const QString& windowClassName, const long winStyle, bool searchSubstringFromBegin = true );

	bool 	IsWindowTitlePresent(HWND hwnd , QString windowCaption);
};

/**
 * @brief Class that contains list of descriptions for blacklisted processes
 */
class BlacklistedProcessesDatabase
{
	static QList< BlacklistedProcessDescription* > m_processesInfo;

protected:
#ifdef BUILD_DEBUG
	static void TestBlackListDatabase();
#endif

	static QString GetWindowTitle( HWND window );
	static bool FindProcessByProcessName( QString processName, BlacklistedProcessDescription** ppProcessInfoResult );
	static bool FindProcessByWindowTitle( DWORD procId, BlacklistedProcessDescription** ppProcessInfoResult );
public:
	// Database initialization and cleanup
	static void InitializeProcessesDatabase();
	static void DeinitializeProcessesDatabase();

	// Search in descriptions list by the process name
	static bool FindProcess(const DWORD procId, QString processName, BlacklistedProcessDescription** ppProcessInfoResult );
};

#endif // OS_WINDOWS

#endif // BLACKLISTEDPROCESSESDATABASE_H
