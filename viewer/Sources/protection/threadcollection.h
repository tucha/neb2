#ifndef THREADCOLLECTION_H
#define THREADCOLLECTION_H

#ifndef OS_WINDOWS
#error "This file must be compiled on Windows OS only"
#endif

#include "../windowsspecific.h"

class ThreadCollection
{
private:
	ThreadCollection();
	Q_DISABLE_COPY( ThreadCollection )
public:
	ThreadCollection( qint64 processId );
	~ThreadCollection();

	bool IsEmpty(void)const;
	bool IsAnyThreadSuspended(void)const;
	bool IsProcessAlive(void)const;

private:
	HANDLE		m_processHandle;
	HandleList	m_threadHandles;
};

#endif // THREADCOLLECTION_H
