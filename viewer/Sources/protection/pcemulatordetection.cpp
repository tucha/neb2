#include "pcemulatordetection.h"

#include <QProcess>

#ifdef OS_MACOSX

PCEmulatorDetection::PCEmulatorDetection()
{
//	const static QString deviceTypes = "SPHardwareDataType SPDisplaysDataType SPMemoryDataType SPParallelATADataType SPSerialATADataType SPUSBDataType";
	const static QString deviceTypes = "SPParallelATADataType SPSerialATADataType SPUSBDataType";

	QProcess systemProfiler;
	systemProfiler.start( "/usr/sbin/system_profiler -detailLevel mini " + deviceTypes );
	systemProfiler.waitForStarted();
	systemProfiler.waitForFinished();
	const QString systemData( systemProfiler.readAllStandardOutput() );
	m_systemLines = systemData.split( '\n', QString::SkipEmptyParts );
	DebugAssert( m_systemLines.isEmpty() == false );
}

bool PCEmulatorDetection::VMWareDetected()
{
	for( int i = 0; i < m_systemLines.size(); ++i )
	{
		if( m_systemLines.at( i ).contains( "vmware", Qt::CaseInsensitive )
		 || m_systemLines.at( i ).contains( "virtual", Qt::CaseInsensitive ) )
		{
			return true;
		}
	}

	return false;
}

bool PCEmulatorDetection::VirtualBoxDetected()
{
	for( int i = 0; i < m_systemLines.size(); ++i )
	{
		if( m_systemLines.at( i ).contains( "vbox", Qt::CaseInsensitive )
		 || m_systemLines.at( i ).contains( "virtualbox", Qt::CaseInsensitive ) )
		{
			return true;
		}
	}

	return false;
}

#endif
