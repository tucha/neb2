#include "ksnapshotprotection.h"

#include <QtCore/QDir>
#include <QtCore/QSettings>
#include <QtCore/QProcess>

QString KSnapshotProtection::KdeHotkeysConfigPath()
{
	QString configPath;
	
	QString homeDir = QDir::homePath();
	if( homeDir.endsWith( QDir::separator() ) == false ) homeDir += QDir::separator();
	configPath += homeDir;
		
	if( QDir( homeDir + ".kde4" ).exists() )
	{
		configPath += ".kde4";
		m_log << "NOTE: .kde4 directory found";
	}
	else if( QDir( homeDir + ".kde" ).exists() )
	{
		configPath += ".kde";
		m_log << "NOTE: .kde directory found";
	}
	else
	{
		m_log << "ERROR: KDE configuration directory not found (~/.kde or ~/.kde4)";
		return QString::null;
	}
	
	configPath += QDir::separator();
	configPath += "share/config/khotkeysrc";		
	if( QFile::exists( configPath ) == false )
	{
		m_log << "ERROR: KDE hotkeys configuration file not found";
		return QString::null;
	}

	m_log << "NOTE: KDE hotkeys configuration file found - " << configPath;
	return configPath;
}

bool KSnapshotProtection::ToggleKSnapshot( bool enabled )
{
	QString enabledString = enabled ? "true" : "false";
	
	// Determine configuration file path
	QString configPath = KdeHotkeysConfigPath();
	if( configPath.isEmpty() ) return false;
	
	// Change setting in the configuration file
	static const QString PRINTSCREEN_KEY = "Data_4_1/Enabled";
	QSettings hotkeysConfig( configPath, QSettings::IniFormat );
	if( hotkeysConfig.contains( PRINTSCREEN_KEY ) == false )
	{
		m_log << "ERROR: KDE hotkeys configuration file has invalid format: Data_4_1/Enabled key is absent";
		return false;
	}
	
	// If PrintScreen option is already disabled - do nothing
	QString currentEnabledString = hotkeysConfig.value( PRINTSCREEN_KEY ).toString();
	if( currentEnabledString.compare( enabledString, Qt::CaseInsensitive ) != 0 )
	{
		hotkeysConfig.setValue( PRINTSCREEN_KEY, enabledString );
		hotkeysConfig.sync();
	}
	else
	{
		m_log << "NOTE: PrintScreen option is already disabled in KDE hotkeys configuration file";
		return true;
	}

	// Reload new settings
	QString dbusSendProgram = "dbus-send";
	QStringList dbusSendArguments;
	dbusSendArguments << "--print-reply" << "--dest=org.kde.kded"
					  << "/modules/khotkeys" << "org.kde.khotkeys.reread_configuration";

	QProcess dbusSendProcess;
	dbusSendProcess.start( dbusSendProgram, dbusSendArguments );
	if( dbusSendProcess.waitForStarted( 1000 ) == false )
	{
		m_log << "ERROR: unable to start dbus-send process";
		return false;
	}
	if( dbusSendProcess.waitForFinished( 1000 ) == false )
	{
		m_log << "ERROR: unable to wait while dbus-send is finished";
		return false;
	}
	if( dbusSendProcess.exitCode() != 0 )
	{
		m_log << "ERROR: dbus-send return non-zero, message: " << dbusSendProcess.readAll();
		return false;
	}

	m_log << "NOTE: KDE hotkeys configuration was successfully updated!";
	return true;
}

KSnapshotProtection::KSnapshotProtection( bool enabled ) :
	m_log(),
	m_configPath( enabled ? KdeHotkeysConfigPath() : "INVALID_PATH" ),
	m_updateResult( enabled ? ToggleKSnapshot( false ) : true )
{
	if( enabled == false ) m_log << "NOTE: ignoring KSnapshot protection";
}

KSnapshotProtection::~KSnapshotProtection()
{
	if( ( m_configPath.isEmpty() == false ) && m_updateResult )
	{
		if( ToggleKSnapshot( true ) ) m_log << "NOTE: KSnapshot protection was successfully disabled";
		else m_log << "ERROR: unable to remove KSnapshot protection";
	}
	else m_log << "WARN: no KSnapshot protection was installed, so no one was removed";
}

bool KSnapshotProtection::IsValid() const
{
	return( ( m_configPath.isEmpty() == false ) && m_updateResult );
}

QStringList KSnapshotProtection::Log() const
{
	return m_log;
}
