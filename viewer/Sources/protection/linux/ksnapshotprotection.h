#ifndef KSNAPSHOTPROTECTION
#define KSNAPSHOTPROTECTION

#include <QStringList>

class KSnapshotProtection
{
	QStringList m_log;
	QString m_configPath;
	bool m_updateResult;

	QString KdeHotkeysConfigPath();
	bool ToggleKSnapshot( bool enabled );

public:
	KSnapshotProtection( bool enabled );
	~KSnapshotProtection();
	
	bool IsValid() const;
	QStringList Log() const;
};

#endif // KSNAPSHOTPROTECTION

