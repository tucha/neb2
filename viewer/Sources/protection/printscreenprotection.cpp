#include "printscreenprotection.h"

#ifdef OS_WINDOWS

#include <QObject>
#include <QMessageBox>

#include <defines.h>

HHOOK PrintScreenProtection::g_getMessageHook = 0;

int PrintScreenProtection::g_altPrtScHookID = 0;
int PrintScreenProtection::g_shiftPrtScHookID = 0;
int PrintScreenProtection::g_ctrPrtScHookID = 0;
int PrintScreenProtection::g_winPrtScHookID = 0;

bool PrintScreenProtection::g_prtScHotKeyWasRegistered = false;
bool PrintScreenProtection::g_altPrtScHotKeyWasRegistered = false;
bool PrintScreenProtection::g_shiftPrtScHotKeyWasRegistered = false;
bool PrintScreenProtection::g_ctrPrtScHotKeyWasRegistered = false;
bool PrintScreenProtection::g_winPrtScHotKeyWasRegistered = false;

QMessageBox* PrintScreenProtection::g_messageBox = NULL;

void PrintScreenProtection::Install()
{
	DebugAssert( g_messageBox == NULL );
	if( g_messageBox != NULL ) return;

	// Register  hotkey  for printscreen button
	g_altPrtScHookID = ( int )GlobalAddAtom( L"IDHOT_ALT_PRINTSCREEN" );
	g_shiftPrtScHookID = ( int )GlobalAddAtom( L"IDHOT_SHIFT_PRINTSCREEN" );
	g_ctrPrtScHookID = ( int )GlobalAddAtom( L"IDHOT_CTR_PRINTSCREEN" );
	g_winPrtScHookID = ( int )GlobalAddAtom( L"IDHOT_WIN_PRINTSCREEN" );

	DebugAssert( g_altPrtScHookID != 0 );
	DebugAssert( g_shiftPrtScHookID != 0 );
	DebugAssert( g_ctrPrtScHookID != 0 );
	DebugAssert( g_winPrtScHookID != 0 );

	g_prtScHotKeyWasRegistered = RegisterHotKey( NULL, IDHOT_SNAPDESKTOP, 0, VK_SNAPSHOT );
	g_altPrtScHotKeyWasRegistered = RegisterHotKey( NULL, g_altPrtScHookID, MOD_ALT, VK_SNAPSHOT );
	g_shiftPrtScHotKeyWasRegistered = RegisterHotKey( NULL, g_shiftPrtScHookID, MOD_SHIFT, VK_SNAPSHOT );
	g_ctrPrtScHotKeyWasRegistered = RegisterHotKey( NULL, g_ctrPrtScHookID, MOD_CONTROL, VK_SNAPSHOT );
	g_winPrtScHotKeyWasRegistered = RegisterHotKey( NULL, g_winPrtScHookID, MOD_WIN, VK_SNAPSHOT );

	// Set hook for hotkey to show message when PrintScreen is used.
	DebugAssert( g_getMessageHook == NULL );
	g_getMessageHook = SetWindowsHookEx( WH_GETMESSAGE, getMessageHookForPrintScreenProtection, NULL, GetCurrentThreadId() );
	DebugAssert( g_getMessageHook != NULL );

	g_messageBox = new QMessageBox();
	g_messageBox->setWindowTitle( QObject::tr( "Print Screen option is disabled." ) );
	g_messageBox->setText( QObject::tr( "To make a snapshot, please close the protected document." ) );
	g_messageBox->setWindowModality( Qt::ApplicationModal );
	g_messageBox->setIcon( QMessageBox::Information );
	g_messageBox->setWindowFlags( Qt::Window | Qt::WindowCloseButtonHint );
}

void PrintScreenProtection::Remove()
{
	DebugAssert( g_messageBox != NULL );
	if( g_messageBox == NULL ) return;

	if( g_getMessageHook != NULL )
	{
		UnhookWindowsHookEx( g_getMessageHook );
		g_getMessageHook = NULL;
	}
	if( g_prtScHotKeyWasRegistered ) UnregisterHotKey( NULL, IDHOT_SNAPDESKTOP );
	if( g_altPrtScHotKeyWasRegistered ) UnregisterHotKey( NULL, g_altPrtScHookID );
	if( g_shiftPrtScHotKeyWasRegistered ) UnregisterHotKey( NULL, g_shiftPrtScHookID );
	if( g_ctrPrtScHotKeyWasRegistered ) UnregisterHotKey( NULL, g_ctrPrtScHookID );
	if( g_winPrtScHotKeyWasRegistered ) UnregisterHotKey( NULL, g_winPrtScHookID );

	g_altPrtScHookID = GlobalDeleteAtom( g_altPrtScHookID );
	g_shiftPrtScHookID = GlobalDeleteAtom( g_shiftPrtScHookID );
	g_ctrPrtScHookID = GlobalDeleteAtom( g_ctrPrtScHookID );
	g_winPrtScHookID = GlobalDeleteAtom( g_winPrtScHookID );

	DeleteAndNull( g_messageBox );
}

LRESULT PrintScreenProtection::getMessageHookForPrintScreenProtection(int code, WPARAM wParam, LPARAM lParam)
{
	if( code == HC_ACTION )
	{
		MSG* msg = ( MSG* )lParam;
		if( msg->message == WM_HOTKEY )
		{
			int hotKeyID = ( int )msg->wParam;
			if( ( hotKeyID == IDHOT_SNAPDESKTOP ) ||
				( hotKeyID == g_altPrtScHookID ) ||
				( hotKeyID == g_shiftPrtScHookID ) ||
				( hotKeyID == g_ctrPrtScHookID ) ||
				( hotKeyID == g_winPrtScHookID ) )
			{
				if( wParam == PM_REMOVE ) g_messageBox->show();
			}
		}
	}

	return CallNextHookEx( 0, code, wParam, lParam );
}

#endif // OS_WINDOWS
