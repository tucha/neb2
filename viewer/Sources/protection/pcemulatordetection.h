#ifndef __PCEMULATORDETECTION
#define __PCEMULATORDETECTION

#include <QStringList>

#include "defines.h"

class PCEmulatorDetection
{
	QStringList m_systemLines;

public:
	PCEmulatorDetection();

	bool VMWareDetected();
	bool VirtualBoxDetected();
};

#endif // __PCEMULATORDETECTION
