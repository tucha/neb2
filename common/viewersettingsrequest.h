#ifndef VIEWERSETTINGSREQUEST_H
#define VIEWERSETTINGSREQUEST_H

#include "apibase.h"
#include "viewersettings.h"

class ViewerSettingsRequest : public ApiBase
{
	Q_OBJECT

public:
	explicit ViewerSettingsRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
			RequestPriority priority = PRIORITY_NORMAL );

	void Send();

signals:
	void SignalFinished( ErrorCode error, ViewerSettings result );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:
	ViewerSettings ParseResult( QByteArray data );
};

#endif // VIEWERSETTINGSREQUEST_H
