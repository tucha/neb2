#ifndef JSONFINDER_H
#define JSONFINDER_H

#include <QString>
#include <QStringList>
#include <QList>
#include <QJsonDocument>

class JsonFinder
{
public:
	JsonFinder( QString data );

	bool IsEmpty(void)const;
	int Count(void)const;
	QJsonDocument Get( int index )const;

private:
	// Private methods
	QStringList Candidates( QString data )const;
	QList<QJsonDocument> Jsons( QStringList candidates )const;

	// Private members
	QString m_data;
	QList<QJsonDocument> m_jsons;
};

#endif // JSONFINDER_H
