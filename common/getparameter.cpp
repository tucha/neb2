#include "getparameter.h"

#include "defines.h"

GetParameter::GetParameter()
{
}

GetParameter::~GetParameter()
{

}

bool GetParameter::CheckRectangle(const QRectF& rectangle)
{
	if( rectangle.left() < 0 ) return false;
	if( rectangle.right() > 1 ) return false;
	if( rectangle.left() >= rectangle.right() ) return false;

	if( rectangle.top() < 0 ) return false;
	if( rectangle.bottom() > 1 ) return false;
	if( rectangle.top() >= rectangle.bottom() ) return false;

	DebugAssert( rectangle.isValid() );
	return true;
}
