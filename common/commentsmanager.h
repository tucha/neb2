#ifndef COMMENTSMANAGER_H
#define COMMENTSMANAGER_H

#include "bookmarksmanagerbase.h"

class CommentsManager : public BookmarksManagerBase
{
	Q_OBJECT

public:
	explicit CommentsManager(QObject *parent, RequestPriority priority = PRIORITY_NORMAL);
	virtual ~CommentsManager();

	QString Get( int pageIndex );

public slots:
	void SendRequest( int pageIndex, QString text );

signals:
	void SignalUpdate( CommentsManager* manager );
    void SignalLoaded( CommentsManager* manager );
protected:
	virtual BookmarksGetRequest*CreateGetRequest();
	virtual BookmarksAddRequest*CreateAddRequest(int pageIndex, QString text);
	virtual BookmarksDeleteRequest*CreateRemoveRequest(int pageIndex);

	virtual Bookmarks Validate(Bookmarks bookmarks);
	virtual Bookmark Validate(Bookmark bookmark);

	virtual void EmitUpdate(void);
    virtual void EmitLoaded(void);

private slots:
	void SlotFinishedModificationStep1(ErrorCode error, Bookmark result);
	void SlotFinishedModificationStep2(ErrorCode error);

private:
	void RequestModification(int pageIndex, QString text);
};

#endif // COMMENTSMANAGER_H
