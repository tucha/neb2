#ifndef REQUESTPAGESET_H
#define REQUESTPAGESET_H

#include "request.h"
#include "pageset.h"

class RequestPageSet : public Request
{
	Q_OBJECT
public:
	RequestPageSet( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
					PageSet pageSet );
	void Send(void);
	PageSet GetPageSet(void)const;

private:
	PageSet m_pageSet;
};

#endif // REQUESTPAGESET_H
