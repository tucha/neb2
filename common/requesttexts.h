#ifndef REQUESTTEXTS_H
#define REQUESTTEXTS_H

#include "requestbase.h"

class RequestTexts : public RequestBase
{
	Q_OBJECT
public:
	RequestTexts(
			QObject *parent,
			QNetworkAccessManager* manager,
			Identifier identifier,
			QString baseUrl,
			QString collection,
			RequestPriority priority = PRIORITY_NORMAL );

	void Send(void);

	QString Collection(void)const;

signals:
	void SignalFinished( ErrorCode error, QString marks );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	const QString m_collection;
};

#endif // REQUESTTEXTS_H
