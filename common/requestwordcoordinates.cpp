#include "requestwordcoordinates.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QVariantMap>

#include "defines.h"

RequestWordCoordinates::RequestWordCoordinates(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
											   int pageIndex, QStringList words):
	RequestPage( parent, manager, identifier, document, priority, pageIndex ),
	m_words( words )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestWordCoordinates::Send()
{
	QString requestString = QString( "%1/%2" )
							.arg( "search" )
							.arg( WordsString( Words() ) );

	RequestPage::Send( requestString );
}

QStringList RequestWordCoordinates::Words() const
{
	return m_words;
}

void RequestWordCoordinates::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParseWordCoordinates( Words(), data ) );
	else emit SignalFinished( error, WordCoordinates() );
}

WordCoordinates RequestWordCoordinates::ParseWordCoordinates(QStringList words, QByteArray data)
{
	QJsonDocument json = QJsonDocument::fromJson( data );
	DebugAssert( json.isNull() == false );

	DebugAssert( json.isArray() );
	QJsonArray array = json.array();

	DebugAssert( words.count() == array.count() );
	if( words.count() != array.count() ) return WordCoordinates();

	WordCoordinates result;
	for( int i = 0; i < array.count(); ++i )
	{
		QString word = words.at( i );

		QJsonValue value = array.at( i );
		DebugAssert( value.isArray() );

		QJsonArray coordinates = value.toArray();
		result.insert( word, ParseWordCoordinatesInternal( word, coordinates ) );
	}
	return result;
}

QList<Word> RequestWordCoordinates::ParseWordCoordinatesInternal(QString word, QJsonArray coordinates)
{
	QList<Word> result;
	for( int i = 0; i < coordinates.count(); ++i )
	{
		QJsonValue value = coordinates.at( i );

		DebugAssert( value.isObject() );
		QJsonObject object = value.toObject();

		QVariantMap map = object.toVariantMap();
		DebugAssert( map.count() == 4 );

		result.append( Word( word, map ) );
	}
	return result;
}
