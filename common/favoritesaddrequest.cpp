#include "favoritesaddrequest.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#define KEY_RESULT_ID "resultID"

FavoritesAddRequest::FavoritesAddRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl,
										 User user, Document document, RequestPriority priority):
	AccountBaseRequest( parent, manager, baseUrl, user, priority ),
	m_document( document )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));

	AddGetParameter( "token", UserToken() );
	AddGetParameter( "book_id", GetDocument().DocumentId() );
}

Document FavoritesAddRequest::GetDocument() const
{
	return m_document;
}

void FavoritesAddRequest::Send()
{
	AccountBaseRequest::Send( "favorites" );
}

void FavoritesAddRequest::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR )
	{
		emit SignalFinished( error, FavoriteBook() );
		return;
	}
	if( data.isEmpty() )
	{
		emit SignalError();
		return;
	}

	emit SignalFinished( error, ParseResult( data ) );
}

QNetworkReply*FavoritesAddRequest::SendInternal(QString requestString)
{
	QNetworkRequest request;
	request.setUrl( QUrl( requestString ) );
	request.setPriority( Priority() );

#if QT_VERSION >= 0x050000
	QByteArray data = GetParameters().query().toUtf8();
#else
	QList<QPair<QString, QString> > prms = GetParameters();

	QByteArray data;
	QUrl url;
	for( QList<QPair<QString, QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		data.append( iter->first + url.queryValueDelimiter() + iter->second + url.queryPairDelimiter() );
	}
	if( data.endsWith( url.queryPairDelimiter() ) ) data.remove( data.length() - 1, 1 );
#endif
	return Manager()->put( request, data );

}

FavoriteBook FavoritesAddRequest::ParseResult(QByteArray data)
{
	QJsonDocument document = QJsonDocument::fromJson( data );

	DebugAssert( document.isObject() );
	if( document.isObject() == false ) return FavoriteBook();
	QJsonObject object = document.object();

	DebugAssert( object.contains( KEY_RESULT_ID ) );
	if( object.contains( KEY_RESULT_ID ) == false ) return FavoriteBook();
	QJsonValue valueId = object.value( KEY_RESULT_ID );

	DebugAssert( valueId.isDouble() );
	if( valueId.isDouble() == false ) return FavoriteBook();
	double id = valueId.toDouble();

	return FavoriteBook( GetUser(), GetDocument(), QString::number( id ) );
}
