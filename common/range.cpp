#include "range.h"

#include <QStringList>

Range::Range():
	m_start( UNDEFINED_VALUE ),
	m_end( UNDEFINED_VALUE )
{
	DebugAssert( IsDefined() == false );
}

Range::Range(int start, int end):
	m_start( start ),
	m_end( end )
{
	DebugAssert( IsDefined() );
}

Range::Range(const Range& another):
	m_start( UNDEFINED_VALUE ),
	m_end( UNDEFINED_VALUE )
{
	CopyFrom( another );
}

Range&Range::operator=(const Range& another)
{
	CopyFrom( another );
	return *this;
}

bool Range::IsDefined() const
{
	bool result = m_start != UNDEFINED_VALUE;
	if( result )
	{
		DebugAssert( m_end != UNDEFINED_VALUE );
		DebugAssert( m_start > 0 );
		DebugAssert( m_end >= m_start  );
	}
	else
	{
		DebugAssert( m_end == UNDEFINED_VALUE );
	}
	return result;
}

int Range::Start() const
{
	DebugAssert( IsDefined() );
	return m_start;
}

int Range::End() const
{
	DebugAssert( IsDefined() );
	return m_end;
}

bool Range::Contains(int value) const
{
	DebugAssert( IsDefined() );
	if( IsDefined() == false ) return false;

	if( value < Start() ) return false;
	if( value > End() ) return false;
	return true;
}

bool Range::IntersectsWith(const Range& another) const
{
	DebugAssert( IsDefined() );
	DebugAssert( another.IsDefined() );
	if( IsDefined() == false ) return false;
	if( another.IsDefined() == false ) return false;

	if( this->Contains( another.Start() ) ) return true;
	if( this->Contains( another.End() ) ) return true;
	return false;
}

Range Range::UnionWith(const Range& another) const
{
	DebugAssert( IsDefined() );
	DebugAssert( another.IsDefined() );
	DebugAssert( IntersectsWith( another ) );
	if( IsDefined() == false ) return Range();
	if( another.IsDefined() == false ) return Range();
	if( IntersectsWith( another ) == false ) return Range();

	int start = Start();
	int end = End();

	if( start > another.Start() ) start = another.Start();
	if( end < another.End() ) end = another.End();
	return Range( start, end );
}

#define SEPARATOR "-"

Range Range::FromString(const QString& string)
{
	QStringList subStrings = string.split( SEPARATOR );

	DebugAssert( subStrings.count() <= 2 );
	if( subStrings.count() > 2 ) return Range();

	if( subStrings.count() == 1 )
	{
		bool ok;
		int start = string.toInt( &ok );

		DebugAssert( ok );
		if( ok == false ) return Range();

		DebugAssert( start >= 0 );
		if( start < 0 ) return Range();

		return Range( start, start );
	}

	bool ok1, ok2;
	int start = subStrings.at( 0 ).trimmed().toInt( &ok1 );
	int end = subStrings.at( 1 ).trimmed().toInt( &ok2 );

	DebugAssert( ok1 && ok2 );
	if( ok1 && ok2 == false ) return Range();

	DebugAssert( start >= 0 );
	if( start < 0 ) return Range();

	DebugAssert( end >= 0 );
	if( end < 0 ) return Range();

	DebugAssert( start <= end );
	if( start > end ) return Range();

	return Range( start, end );
}

bool Range::CheckString(const QString& string)
{
	if( string.isEmpty() ) return false;

	QStringList subStrings = string.split( SEPARATOR );
	if( subStrings.count() > 2 ) return false;

	if( subStrings.count() == 1 )
	{
		bool ok;
		int start = string.toInt( &ok );

		if( ok == false ) return false;
		if( start < 0 ) return false;
		return true;
	}

	bool ok1, ok2;
	int start = subStrings.at( 0 ).trimmed().toInt( &ok1 );
	int end = subStrings.at( 1 ).trimmed().toInt( &ok2 );
	if( ok1 && ok2 == false ) return false;
	if( start < 0 ) return false;
	if( end < start ) return false;

	return true;
}

QString Range::ToString() const
{
	if( Start() == End() ) return QString::number( Start() );
	return QString( "%1%2%3" )
			.arg( Start() )
			.arg( SEPARATOR )
			.arg( End() );
}

QVector<int> Range::ToList() const
{
	QVector<int> result;

	for( int i = Start(); i <= End(); ++i )
	{
		result.append( i );
	}

	return result;
}

void Range::CopyFrom(const Range& another)
{
	m_start = another.m_start;
	m_end = another.m_end;
	DebugAssert( IsDefined() == another.IsDefined() );
}
