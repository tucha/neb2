#ifndef RANGE_H
#define RANGE_H

#include <QString>
#include <QVector>

#include "defines.h"

#define UNDEFINED_VALUE -1

class Range
{
public:
	Range(void);
	Range( int start, int end );
	Range( const Range& another );
	Range& operator=( const Range& another );

	bool IsDefined(void)const;
	int Start(void)const;
	int End(void)const;

	bool Contains( int value )const;
	bool IntersectsWith( const Range& another )const;
	Range UnionWith( const Range& another )const;

	static Range FromString( const QString& string );
	static bool CheckString( const QString& string );
	QString ToString(void)const;

	QVector<int> ToList(void)const;

private:
	void CopyFrom( const Range& another );
	int m_start;
	int m_end;
};

#endif // RANGE_H
