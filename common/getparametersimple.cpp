#include "getparametersimple.h"

GetParameterSimple::GetParameterSimple(QString key, QString value):
	GetParameter(),
	m_key( key ),
	m_value( value )
{

}

GetParameterSimple::~GetParameterSimple()
{

}

QString GetParameterSimple::Key() const
{
	return m_key;
}

QString GetParameterSimple::Value() const
{
	return m_value;
}
