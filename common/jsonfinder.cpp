#include "jsonfinder.h"

#include "defines.h"

#define JSON_START	'{'
#define JSON_END	'}'

JsonFinder::JsonFinder(QString data):
	m_data( data ),
	m_jsons()
{
	QStringList candidates = Candidates( data );
	QList<QJsonDocument> jsons = Jsons( candidates );

	m_jsons.append( jsons );
}

bool JsonFinder::IsEmpty() const
{
	return Count() == 0;
}

int JsonFinder::Count() const
{
	return m_jsons.count();
}

QJsonDocument JsonFinder::Get(int index) const
{
	DebugAssert( index >= 0 && index < Count() );
	if( index < 0 || index >= Count() ) return QJsonDocument();

	return m_jsons.at( index );
}

QStringList JsonFinder::Candidates(QString data) const
{
	QStringList result;

	int start = -1;
	int counter = 0;
	for( int i = 0; i < data.length(); ++i )
	{
		if( start == -1 )
		{
			if( data.at( i ) != JSON_START ) continue;
			start = i;
			counter = 1;
		}
		else
		{
			if( data.at( i ) == JSON_START )
			{
				counter += 1;
				continue;
			}
			if( data.at( i ) == JSON_END )
			{
				DebugAssert( counter > 0 );

				counter -= 1;
				if( counter != 0 ) continue;

				result.append( data.mid( start, i - start + 1 ) );
				start = -1;
				counter = 0;
			}
		}
	}

	return result;
}

QList<QJsonDocument> JsonFinder::Jsons(QStringList candidates) const
{
	QList<QJsonDocument> result;

	foreach( QString candidate, candidates )
	{
		QJsonDocument json = QJsonDocument::fromJson( candidate.toUtf8() );
		if( json.isNull() ) continue;
		result.append( json );
	}

	return result;
}
