#include "requesttexts.h"

RequestTexts::RequestTexts(QObject* parent, QNetworkAccessManager* manager, Identifier identifier, QString baseUrl, QString collection, RequestPriority priority):
	RequestBase( parent, manager, identifier, baseUrl, priority ),
	m_collection( collection )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestTexts::Send()
{
	QString request = QString( "%1/%2/%3" )
					  .arg( "collections" )
					  .arg( Collection() )
					  .arg( "texts" );
	ApiBase::Send( request );
}

QString RequestTexts::Collection() const
{
	return m_collection;
}

void RequestTexts::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR ) emit SignalFinished( error, QString() );
	else emit SignalFinished( error, data );
}
