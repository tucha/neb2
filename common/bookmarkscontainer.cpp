#include "bookmarkscontainer.h"

#include "defines.h"

BookmarksContainer::BookmarksContainer( Bookmarks bookmarks ):
	m_user(),
	m_document(),
	m_comments( false ),
	m_pages(),
	m_ids()
{
	DebugAssert( IsEmpty() );
	foreach( Bookmark b, bookmarks ) Add( b );

	DebugAssert( Count() == bookmarks.count() );
	if( Count() != bookmarks.count() ) Clear();
}

bool BookmarksContainer::IsEmpty() const
{
	bool result = Count() == 0;
	if( result )
	{
		DebugAssert( m_user.IsEmpty() );
		DebugAssert( m_document.IsEmpty() );
		DebugAssert( m_comments == false );
		DebugAssert( m_pages.isEmpty() );
		DebugAssert( m_ids.isEmpty() );
	}
	else
	{
		DebugAssert( m_user.IsEmpty() == false );
		DebugAssert( m_document.IsEmpty() == false );
		DebugAssert( m_pages.isEmpty() == false );
		DebugAssert( m_ids.count() == m_pages.count() );
	}
	return result;
}

bool BookmarksContainer::Contains(int pageIndex) const
{
	DebugAssert( pageIndex >= 0 );
	if( pageIndex < 0 ) return false;

	return m_pages.contains( pageIndex );
}

bool BookmarksContainer::Add(Bookmark bookmark)
{
	DebugAssert( bookmark.IsEmpty() == false );
	if( bookmark.IsEmpty() ) return false;

	if( IsEmpty() == false )
	{
		DebugAssert( Check( bookmark ) );
		if( Check( bookmark ) == false ) return false;

		DebugAssert( Contains( bookmark.PageIndex() ) == false );
		if( Contains( bookmark.PageIndex() ) ) return false;
	}
	else
	{
		m_user = bookmark.GetUser();
		m_document = bookmark.GetDocument();
		m_comments = bookmark.IsComment();
	}

	m_pages.insert( bookmark.PageIndex(), bookmark );
	m_ids.insert( bookmark.Id() );
	return true;
}

Bookmark BookmarksContainer::Get(int pageIndex) const
{
	DebugAssert( pageIndex >= 0 );
	return m_pages.value( pageIndex, Bookmark() );
}

bool BookmarksContainer::Remove(Bookmark bookmark)
{
	DebugAssert( bookmark.IsEmpty() == false );
	if( bookmark.IsEmpty() ) return false;

	if( Contains( bookmark.PageIndex() ) == false ) return false;

	m_pages.remove( bookmark.PageIndex() );
	m_ids.remove( bookmark.Id() );

	if( m_pages.isEmpty() )
	{
		m_user = User();
		m_document = Document();
		m_comments = false;
		DebugAssert( IsEmpty() );
	}

	return true;
}

int BookmarksContainer::Count() const
{
	return m_pages.count();
}

void BookmarksContainer::Clear()
{
	*this = BookmarksContainer();
	DebugAssert( IsEmpty() );
}

QList<int> BookmarksContainer::Pages() const
{
	return m_pages.keys();
}

bool BookmarksContainer::Check(Bookmark bookmark) const
{
	if( IsEmpty() ) return true;

	if( m_user.GetToken() != bookmark.GetUser().GetToken() ) return false;
	if( m_document != bookmark.GetDocument() ) return false;
	if( m_comments != bookmark.IsComment() ) return false;
	if( m_ids.contains( bookmark.Id() ) ) return false;
	return true;
}
