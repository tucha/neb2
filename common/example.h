#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <QMainWindow>
#include <QString>

class QScrollArea;
class QVBoxLayout;
class QHBoxLayout;
class QNetworkAccessManager;
class QNetworkProxy;

#include "common.h"
#include "user.h"
#include "identifier.h"
#include "bookmarksmanager.h"
#include "commentsmanager.h"

class Example : public QMainWindow
{
	Q_OBJECT
public:
	// Constructor
	explicit Example(QWidget *parent = 0);

private:
	// Private methods
	void CreateGui(void);
	void StartAuthentication(void);
	void StartDownloading(void);
	void StartAccountTest(void);
	void Add1( QString text, int maxHeight = 100 );
	void Add2( QImage image );
	void Add3( QString text, int maxHeight = 100);
	Identifier CreateId(void)const;

private slots:
	void SlotFinishedCollections( ErrorCode error, QStringList collections );
	void SlotFinishedMarks( ErrorCode error, QString marks );
	void SlotFinishedTexts( ErrorCode error, QString texts );

	// Private slots (for proxy and http authorization)
	void SlotProxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *authenticator);
	void SlotAuthenticationRequired( QAuthenticator* authenticator );

	// Private slots (for authentication)
	void SlotAuthorizationRequestFinished( ErrorCode error, AuthorizationResult result );
	void SlotAuthorizationCheckFinished( ErrorCode error, AuthorizationResult result );

	// Private slots (for all document)
	void SlotFinishedFileExtensions( ErrorCode error, QStringList fileExtensions );
	void SlotFinishedFile( ErrorCode error, QByteArray fileData );
	void SlotFinishedFileSize( ErrorCode error, int fileSize );
	void SlotFinishedDescription( ErrorCode error, QString description );
	void SlotFinishedInformation( ErrorCode error, QVariantMap information );
	void SlotFinishedDocumentType( ErrorCode error, QString documentType );
	void SlotFinishedCollection( ErrorCode error, QString collection );
	void SlotFinishedPagesCount( ErrorCode error, int pagesCount );
	void SlotFinishedPageSet( ErrorCode error, QByteArray pageSet );
	void SlotFinishedWordPages( ErrorCode error, WordPages wordPages );

	// Private slots (for pages)
	void SlotFinishedPageSize( ErrorCode error, QSize pageSize );
	void SlotFinishedImage( ErrorCode error, QImage image );
	void SlotFinishedImageFixed( ErrorCode error, QImage image );
	void SlotFinishedWords( ErrorCode error, QList<Word> words );
	void SlotFinishedWordCoordinates( ErrorCode error, WordCoordinates wordCoordinates );
	void SlotFinishedImageWithWords( ErrorCode error, QImage image );

	// Private slots (for account)
	void BookmarksUpdate(BookmarksManager* manager);
	void CommentsUpdate(CommentsManager* manager);

private:
	// Private members
	QNetworkAccessManager* const m_manager;
	User m_user;
	BookmarksManager m_bookmarks;
	CommentsManager m_comments;

	// Gui items
	QScrollArea* m_scrollArea;
	QHBoxLayout* m_horizontalBoxLayout;
	QVBoxLayout* m_verticalBoxLayout1;
	QVBoxLayout* m_verticalBoxLayout2;
	QVBoxLayout* m_verticalBoxLayout3;
	QWidget* m_widget;
	QWidget* m_widget1;
	QWidget* m_widget2;
	QWidget* m_widget3;
};

#endif // EXAMPLE_H
