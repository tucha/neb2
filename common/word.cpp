#include "word.h"

#include "defines.h"

Word::Word():
	m_rectangle(),
	m_text()
{

}

Word::Word(QRectF rectangle, QString text):
	m_rectangle( rectangle ),
	m_text( text )
{

}

Word::Word(QVariantMap map):
	m_rectangle(),
	m_text()
{
	QRectF rectangle = GetRectangle( map );
	if( rectangle.isValid() ) m_rectangle = rectangle;

	QString text = GetText( map );
	if( text.isEmpty() == false ) m_text = text;
}

Word::Word(QString text, QVariantMap map):
	m_rectangle(),
	m_text()
{
	QRectF rectangle = GetRectangle( map );
	if( rectangle.isValid() ) m_rectangle = rectangle;

	m_text = text;
}

QRectF Word::Rectangle() const
{
	return m_rectangle;
}

QString Word::Text() const
{
	return m_text;
}

QRectF Word::GetRectangle(QVariantMap map)
{
	const QString x1Key = "x1";
	const QString x2Key = "x2";
	const QString y1Key = "y1";
	const QString y2Key = "y2";
	DebugAssert( map.contains( x1Key ) );
	DebugAssert( map.contains( x2Key ) );
	DebugAssert( map.contains( y1Key ) );
	DebugAssert( map.contains( y2Key ) );

	QRectF rectangle;

	bool ok;
	if( map.contains( x1Key ) )
	{
		rectangle.setLeft( map.value( x1Key ).toFloat( &ok ) );
		DebugAssert( ok );
	}
	if( map.contains( x2Key ) )
	{
		rectangle.setRight( map.value( x2Key ).toFloat( &ok ) );
		DebugAssert( ok );
	}
	if( map.contains( y1Key ) )
	{
		rectangle.setTop( map.value( y1Key ).toFloat( &ok ) );
		DebugAssert( ok );
	}
	if( map.contains( y2Key ) )
	{
		rectangle.setBottom( map.value( y2Key ).toFloat( &ok ) );
		DebugAssert( ok );
	}

	DebugAssert( rectangle.isValid() );
	return rectangle;
}

QString Word::GetText(QVariantMap map)
{
	const QString textKey  = "text";
	DebugAssert( map.contains( textKey ) );

	QString text;
	if( map.contains( textKey ) )
	{
		text = map.value( textKey ).toString();
	}

	DebugAssert( text.isEmpty() == false );
	return text;
}
