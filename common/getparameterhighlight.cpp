#include "getparameterhighlight.h"

#include "defines.h"

GetParameterHighlight::GetParameterHighlight(const QRectF& rectangle):
	GetParameter(),
	m_rectangle( rectangle )
{

}

GetParameterHighlight::~GetParameterHighlight()
{

}

QString GetParameterHighlight::Key() const
{
	return "annot";
}

QString GetParameterHighlight::Value() const
{
	DebugAssert( CheckRectangle( Rectangle() ) );
	if( CheckRectangle( Rectangle() ) == false ) return QString();

	return QString( "%1,%2,%3,%4" )
			.arg( Rectangle().left() )
			.arg( Rectangle().top() )
			.arg( Rectangle().right() )
			.arg( Rectangle().bottom() );
}

QRectF GetParameterHighlight::Rectangle() const
{
	return m_rectangle;
}
