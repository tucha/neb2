#ifndef REQUESTWORDCOORDINATES_H
#define REQUESTWORDCOORDINATES_H

#include "requestpage.h"
#include "word.h"

#include <QList>
#include <QByteArray>
#include <QHash>
#include <QStringList>
#include <QJsonArray>

typedef QHash<QString,QList<Word> > WordCoordinates;

class RequestWordCoordinates : public RequestPage
{
	Q_OBJECT
public:
	RequestWordCoordinates( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
					int pageIndex, QStringList words );
	void Send(void);
	QStringList Words(void)const;

signals:
	void SignalFinished( ErrorCode error, WordCoordinates wordCoordinates );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	WordCoordinates ParseWordCoordinates(QStringList words, QByteArray data);
	QList<Word> ParseWordCoordinatesInternal(QString word, QJsonArray coordinates);

	QStringList m_words;
};

#endif // REQUESTWORDCOORDINATES_H
