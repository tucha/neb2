#include "bookmarksmanager.h"

#include "defines.h"

BookmarksManager::BookmarksManager(QObject* parent, RequestPriority priority) :
	BookmarksManagerBase( parent, priority )
{
}

BookmarksManager::~BookmarksManager()
{
	Clear();
}

void BookmarksManager::SendRequest(int pageIndex, bool bookmark)
{
	DebugAssert( pageIndex >= 0 );

	DebugAssert( IsActual( pageIndex ) );
	if( IsActual( pageIndex ) == false ) return;

	if( bookmark ) RequestAddition( pageIndex );
	else RequestRemoval( pageIndex );
}

BookmarksGetRequest*BookmarksManager::CreateGetRequest()
{
	return BaseRequest()->NewBookmarksGetForAllPages( GetDocument(), Priority() );
}

BookmarksAddRequest*BookmarksManager::CreateAddRequest(int pageIndex, QString text)
{
	UNUSED( text );
	DebugAssert( text.isEmpty() );
	return BaseRequest()->NewBookmarksAdd( GetDocument(), pageIndex, Priority() );
}

BookmarksDeleteRequest*BookmarksManager::CreateRemoveRequest(int pageIndex)
{
	return BaseRequest()->NewBookmarksDelete( m_bookmarks.Get( pageIndex ), Priority() );
}

Bookmarks BookmarksManager::Validate(Bookmarks bookmarks)
{
	return bookmarks;
}

Bookmark BookmarksManager::Validate(Bookmark bookmark)
{
	return bookmark;
}

void BookmarksManager::EmitUpdate()
{
	emit SignalUpdate( this );
}

void BookmarksManager::EmitLoaded()
{
    emit SignalLoaded( this );
}
