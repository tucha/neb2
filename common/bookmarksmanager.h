#ifndef BOOKMARKSMANAGER_H
#define BOOKMARKSMANAGER_H

#include "bookmarksmanagerbase.h"

class BookmarksManager : public BookmarksManagerBase
{
	Q_OBJECT

public:
	explicit BookmarksManager(QObject *parent, RequestPriority priority = PRIORITY_NORMAL);
	virtual ~BookmarksManager();

public slots:
	void SendRequest( int pageIndex, bool bookmark );

signals:
	void SignalUpdate( BookmarksManager* manager );
    void SignalLoaded( BookmarksManager* manager );
protected:
	virtual BookmarksGetRequest*CreateGetRequest();
	virtual BookmarksAddRequest*CreateAddRequest(int pageIndex, QString text);
	virtual BookmarksDeleteRequest*CreateRemoveRequest(int pageIndex);

	virtual Bookmarks Validate(Bookmarks bookmarks);
	virtual Bookmark Validate(Bookmark bookmark);

	virtual void EmitUpdate(void);
    virtual void EmitLoaded(void);
};

#endif // BOOKMARKSMANAGER_H
