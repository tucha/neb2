#ifndef PAGESET_H
#define PAGESET_H

#include <QVector>
#include <QString>

#include "range.h"

#include "defines.h"

class PageSet
{
public:
	PageSet();
	PageSet( const PageSet& another );
	PageSet& operator=( const PageSet& another );
	PageSet( int startPage, int endPage );

	void Add( int page );
	void Add(const Range& newRange );

	static PageSet FromString( const QString& string );
	static bool CheckString( const QString& string );
	QString ToString(void)const;

	bool IsEmpty(void)const;

	QVector<int> ToList(void)const;

private:
	bool Test(void)const;
	QVector<Range> MergeRanges( QVector<Range> ranges );
	QVector<Range> m_ranges;
};

#endif // PAGESET_H
