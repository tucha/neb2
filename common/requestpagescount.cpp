#include "requestpagescount.h"

#include "defines.h"

RequestPagesCount::RequestPagesCount(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority):
	Request( parent, manager, identifier, document, priority )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestPagesCount::Send()
{
	QString requestString = QString( "%1/%2/%3" )
							.arg( "document" )
							.arg( "pages" )
							.arg( "count" );
	ApiBase::Send( requestString );
}

void RequestPagesCount::SlotFinished(ErrorCode error, QByteArray data)
{
	emit SignalFinished( error, data.toInt() );
}
