#ifndef FAVORITESDELETEREQUEST_H
#define FAVORITESDELETEREQUEST_H

#include "accountbaserequest.h"
#include "favoritebook.h"

class FavoritesDeleteRequest : public AccountBaseRequest
{
	Q_OBJECT
public:
	explicit FavoritesDeleteRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
								 FavoriteBook favoriteBook, RequestPriority priority = PRIORITY_NORMAL );

	FavoriteBook GetFavoriteBook(void)const;

	void Send(void);

signals:
	void SignalFinished( ErrorCode error );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:
	const FavoriteBook m_favoriteBook;
};

#endif // FAVORITESDELETEREQUEST_H
