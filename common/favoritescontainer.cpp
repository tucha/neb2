#include "favoritescontainer.h"

#include "defines.h"

FavoritesContainer::FavoritesContainer():
	m_favoriteBooks(),
	m_user(),
	m_ids()
{
	DebugAssert( IsEmpty() );
}

FavoritesContainer::FavoritesContainer(FavoriteBooks favoriteBooks):
	m_favoriteBooks(),
	m_user(),
	m_ids()
{
	foreach( FavoriteBook b, favoriteBooks ) Add( b );

	DebugAssert( m_favoriteBooks.count() == favoriteBooks.count() );
	if( m_favoriteBooks.count() != favoriteBooks.count() ) Clear();
}

bool FavoritesContainer::IsEmpty() const
{
	bool result = m_favoriteBooks.isEmpty();

	if( result )
	{
		DebugAssert( m_favoriteBooks.isEmpty() );
		DebugAssert( m_user.IsEmpty() );
		DebugAssert( m_ids.isEmpty() );
	}
	else
	{
		DebugAssert( m_favoriteBooks.isEmpty() == false );
		DebugAssert( m_user.IsEmpty() == false );
		DebugAssert( m_ids.count() == m_favoriteBooks.count() );
	}

	return result;
}

bool FavoritesContainer::Add(FavoriteBook favoriteBook)
{
	DebugAssert( favoriteBook.IsEmpty() == false );
	if( favoriteBook.IsEmpty() ) return false;

	if( IsEmpty() == false )
	{
		DebugAssert( Check( favoriteBook ) );
		if( Check( favoriteBook ) == false ) return false;

		DebugAssert( Contains( favoriteBook.GetDocument() ) == false );
		if( Contains( favoriteBook.GetDocument() ) ) return false;
	}
	else
	{
		m_user = favoriteBook.GetUser();
	}

	m_favoriteBooks.insert( favoriteBook.GetDocument(), favoriteBook );
	m_ids.insert( favoriteBook.Id(), favoriteBook.GetDocument() );
	return true;
}

bool FavoritesContainer::Remove(Document document)
{
	if( m_favoriteBooks.contains( document ) == false ) return false;

	FavoriteBook favoriteBook = m_favoriteBooks.value( document );
	m_favoriteBooks.remove( document );
	m_ids.remove( favoriteBook.Id() );
	if( m_favoriteBooks.isEmpty() ) m_user = User();
	return true;
}

bool FavoritesContainer::Contains(Document document) const
{
	return m_favoriteBooks.contains( document );
}

DocumentList FavoritesContainer::Documents() const
{
	return m_ids.values(); // QMap is used to sort favorite documents by their ID
}

void FavoritesContainer::Clear()
{
	*this = FavoritesContainer();
}

FavoriteBook FavoritesContainer::Get(Document document)
{
	return m_favoriteBooks.value( document, FavoriteBook() );
}

bool FavoritesContainer::Check(FavoriteBook favoriteBook)
{
	if( IsEmpty() ) return true;

	if( m_user.GetToken() != favoriteBook.GetUser().GetToken() ) return false;
	if( m_ids.contains( favoriteBook.Id() ) ) return false;
	return true;
}
