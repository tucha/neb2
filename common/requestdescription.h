#ifndef REQUESTDESCRIPTION_H
#define REQUESTDESCRIPTION_H

#include "request.h"

class RequestDescription : public Request
{
	Q_OBJECT
public:
	RequestDescription( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
						QString field, QString subField = "" );
	void Send(void);

	QString Field(void)const;
	QString SubField(void)const;

signals:
	void SignalFinished( ErrorCode error, QString description );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	QString ParseData( QByteArray data );

	const QString m_field;
	const QString m_subField;
};

#endif // REQUESTDESCRIPTION_H
