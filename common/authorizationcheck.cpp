#include "authorizationcheck.h"

AuthorizationCheck::AuthorizationCheck(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, RequestPriority priority, QString token):
	AuthorizationBase( parent, manager, baseUrl, priority ),
	m_token( token )
{
	AddGetParameter( "token", token );
}

void AuthorizationCheck::Send()
{
	AuthorizationBase::Send( "check_token" );
}

QString AuthorizationCheck::Token() const
{
	return m_token;
}
