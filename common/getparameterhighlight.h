#ifndef GETPARAMETERHIGHLIGHT_H
#define GETPARAMETERHIGHLIGHT_H

#include "getparameter.h"

class GetParameterHighlight : public GetParameter
{
public:
	explicit GetParameterHighlight( const QRectF& rectangle );
	virtual ~GetParameterHighlight();
	virtual QString Key(void)const;
	virtual QString Value(void)const;

	QRectF Rectangle(void)const;

private:
	const QRectF m_rectangle;
};

#endif // GETPARAMETERHIGHLIGHT_H
