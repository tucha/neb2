#ifndef BOOKMARKSCONTAINER_H
#define BOOKMARKSCONTAINER_H

#include <QMap>
#include <QSet>

#include "bookmark.h"

// Ensures that all bookmarks are unique and from the same user and document
class BookmarksContainer
{
public:
	BookmarksContainer( Bookmarks bookmarks = Bookmarks() );

	bool IsEmpty(void)const;
	bool Contains( int pageIndex )const;
	bool Add( Bookmark bookmark );
	Bookmark Get( int pageIndex )const;
	bool Remove( Bookmark bookmark );
	int Count(void)const;
	void Clear(void);
	QList<int> Pages(void)const;

private:
	bool Check( Bookmark bookmark )const;

	User m_user;
	Document m_document;
	bool m_comments;
	QMap<int,Bookmark> m_pages;
	QSet<QString> m_ids;
};

#endif // BOOKMARKSCONTAINER_H
