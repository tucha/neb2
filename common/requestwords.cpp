#include "requestwords.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>

RequestWords::RequestWords(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority, int pageIndex):
	RequestPage( parent, manager, identifier, document, priority, pageIndex )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestWords::Send()
{
	RequestPage::Send( "wordList" );
}

void RequestWords::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParseWords( data ) );
	else emit SignalFinished( error, QList<Word>() );
}

QList<Word> RequestWords::ParseWords(QByteArray data)
{
	if( data.startsWith( NOT_FOUND ) ) return QList<Word>();

	QJsonDocument json = QJsonDocument::fromJson( data );
	DebugAssert( json.isNull() == false );

	DebugAssert( json.isArray() );
	QJsonArray array = json.array();

	QList<Word> result;
	for( int i = 0; i < array.count(); ++i )
	{
		QJsonValue value = array.at( i );

		DebugAssert( value.isObject() );
		QJsonObject object = value.toObject();

		QVariantMap map = object.toVariantMap();
		DebugAssert( map.count() == 5 );

		result.append( Word( map ) );
	}
	return result;
}
