#ifndef REQUESTFILESIZE_H
#define REQUESTFILESIZE_H

#include "request.h"

class RequestFileSize : public Request
{
	Q_OBJECT
public:
	RequestFileSize( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
				 QString fileExtension );
	void Send(void);
	QString FileExtension(void)const;

signals:
	void SignalFinished( ErrorCode error, int fileSize );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	const QString m_fileExtension;
};

#endif // REQUESTFILESIZE_H
