#ifndef FAVORITESADDREQUEST_H
#define FAVORITESADDREQUEST_H

#include "accountbaserequest.h"
#include "favoritebook.h"

class FavoritesAddRequest : public AccountBaseRequest
{
	Q_OBJECT
public:
	explicit FavoritesAddRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
								 User user, Document document, RequestPriority priority = PRIORITY_NORMAL );

	Document GetDocument(void)const;

	void Send(void);

signals:
	void SignalFinished( ErrorCode error, FavoriteBook result );
	void SignalError();

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:
	FavoriteBook ParseResult( QByteArray data );

	const Document m_document;
};

#endif // FAVORITESADDREQUEST_H
