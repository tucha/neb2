#include "requestfileextensions.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>

#include "defines.h"

RequestFileExtensions::RequestFileExtensions(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority):
	Request( parent, manager, identifier, document, priority )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestFileExtensions::Send(void)
{
	ApiBase::Send( "resources" );
}

void RequestFileExtensions::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParseData( data ) );
	else emit SignalFinished( error, QStringList() );
}

QStringList RequestFileExtensions::ParseData(QByteArray data)
{
	QJsonDocument json = QJsonDocument::fromJson( data );
	DebugAssert( json.isNull() == false );

	DebugAssert( json.isArray() );
	QJsonArray array = json.array();

	QStringList result;
	for( int i = 0; i < array.count(); ++i )
	{
		QJsonValue value = array.at( i );
		QString str = value.toString();
		result.append( str );
	}
	return result;
}
