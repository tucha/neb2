#ifndef STATUSBASEREQUEST_H
#define STATUSBASEREQUEST_H

#include "apibase.h"
#include "user.h"

class StatusRequest : public ApiBase
{
	Q_OBJECT
public:
	enum RequestType
	{
		CheckUser,
		CheckIp,
	};

	explicit StatusRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
			User user, RequestType type, RequestPriority priority = PRIORITY_NORMAL );

	User GetUser(void)const;
	RequestType GetType(void)const;

	void Send();

signals:
	void SignalFinished( ErrorCode error, bool result );
	void SignalFinishedUserStatus( ErrorCode, User::UserStatus status );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	void FinishedUserStatus( ErrorCode error, QByteArray data );
	void FinishedIpCheck( ErrorCode error, QByteArray data );
	QString GetAction(void)const;
	QString GetQuestion(void)const;

	const User m_user;
	const RequestType m_type;

protected:
	virtual QNetworkReply* SendInternal( QString requestString );
};

#endif // STATUSBASEREQUEST_H
