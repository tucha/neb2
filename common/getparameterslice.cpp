#include "getparameterslice.h"

#include "defines.h"

GetParameterSlice::GetParameterSlice(const QRectF& rectangle):
	GetParameter(),
	m_rectangle( rectangle )
{

}

GetParameterSlice::~GetParameterSlice()
{

}

QString GetParameterSlice::Key() const
{
	return "slice";
}

QString GetParameterSlice::Value() const
{
	DebugAssert( CheckRectangle( Rectangle() ) );
	if( CheckRectangle( Rectangle() ) == false ) return QString();

	return QString( "%1,%2,%3,%4" )
			.arg( Rectangle().left() )
			.arg( Rectangle().top() )
			.arg( Rectangle().width() )
			.arg( Rectangle().height() );
}

QRectF GetParameterSlice::Rectangle() const
{
	return m_rectangle;
}
