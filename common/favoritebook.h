#ifndef FAVORITEBOOK_H
#define FAVORITEBOOK_H

#include <QString>

#include "user.h"
#include "document.h"

class FavoriteBook;
typedef QList<FavoriteBook> FavoriteBooks;

class FavoriteBook
{
public:
	FavoriteBook();
	FavoriteBook( User user, Document document, QString id );

	bool IsEmpty(void)const;

	User GetUser(void)const;
	Document GetDocument(void)const;
	QString Id(void)const;

private:
	User m_user;
	Document m_document;
	QString m_id;
};

#endif // FAVORITEBOOK_H
