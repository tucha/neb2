#ifndef AUTHORIZATIONREQUEST_H
#define AUTHORIZATIONREQUEST_H

#include "authorizationbase.h"

class AuthorizationRequest: public AuthorizationBase
{
	Q_OBJECT
public:
	AuthorizationRequest( QObject *parent, QNetworkAccessManager* manager, QString baseUrl, RequestPriority priority,
						  QString email, QString password );

	void Send(void);

	QString Email(void)const;
	QString Password(void)const;

private:
	QString PercentsEncoded(QByteArray bytes);
	const QString m_email;
	const QString m_password;
};

#endif // AUTHORIZATIONREQUEST_H
