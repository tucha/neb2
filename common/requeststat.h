#ifndef REQUESTSTAT_H
#define REQUESTSTAT_H

#include "apibase.h"

#include <QDate>

class RequestStat : public ApiBase
{
	Q_OBJECT
public:
	RequestStat(QObject *parent,
			QNetworkAccessManager* manager,
			QString baseUrl,
			QDate from,
			QDate to,
			RequestPriority priority = PRIORITY_NORMAL );

	void Send(void);

	QDate From(void)const;
	QDate To(void)const;

signals:
	void SignalFinished( ErrorCode error, QString marks );

protected:
	virtual QNetworkReply* SendInternal(QString requestString);

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	const QDate m_from;
	const QDate m_to;
};

#endif // REQUESTSTAT_H
