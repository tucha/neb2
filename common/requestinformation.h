#ifndef REQUESTINFORMATION_H
#define REQUESTINFORMATION_H

#include "request.h"

#include <QVariantMap>

class RequestInformation : public Request
{
	Q_OBJECT
public:
	RequestInformation( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority );
	void Send(void);

signals:
	void SignalFinished( ErrorCode error, QVariantMap information );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	QVariantMap ParseInformation(QByteArray data);

};


#endif // REQUESTINFORMATION_H
