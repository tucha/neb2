#ifndef GETPARAMETER_H
#define GETPARAMETER_H

#include <QString>
#include <QRectF>

class GetParameter
{
public:
	GetParameter();
	virtual ~GetParameter();
	virtual QString Key(void)const = 0;
	virtual QString Value(void)const = 0;

protected:
	static bool CheckRectangle( const QRectF& rectangle );
};

#endif // GETPARAMETER_H
