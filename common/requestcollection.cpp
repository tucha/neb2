#include "requestcollection.h"

#include "defines.h"

RequestCollection::RequestCollection(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority):
	Request( parent, manager, identifier, document, priority )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestCollection::Send()
{
	QString requestString = QString( "%1/%2" )
							.arg( "document" )
							.arg( "collection" );
	ApiBase::Send( requestString );
}

void RequestCollection::SlotFinished(ErrorCode error, QByteArray data)
{
	emit SignalFinished( error, data );
}
