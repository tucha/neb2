#ifndef REQUESTWORDS_H
#define REQUESTWORDS_H

#include "requestpage.h"
#include "word.h"

#include <QList>
#include <QByteArray>

class RequestWords : public RequestPage
{
	Q_OBJECT
public:
	RequestWords( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
					int pageIndex );
	void Send(void);

signals:
	void SignalFinished( ErrorCode error, QList<Word> words );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	QList<Word> ParseWords(QByteArray data);
};

#endif // REQUESTWORDS_H
