#include "requestfilesize.h"

RequestFileSize::RequestFileSize(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority, QString fileExtension):
	Request( parent, manager, identifier, document, priority ),
	m_fileExtension( fileExtension )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestFileSize::Send()
{
	QString requestString = QString( "%1/%2/%3" )
							.arg( "resources" )
							.arg( FileExtension() )
							.arg( "size" );
	ApiBase::Send( requestString );
}

QString RequestFileSize::FileExtension() const
{
	return m_fileExtension;
}

void RequestFileSize::SlotFinished(ErrorCode error, QByteArray data)
{
	emit SignalFinished( error, data.toInt() );
}
