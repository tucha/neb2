#include "commentsmanager.h"

CommentsManager::CommentsManager(QObject* parent, RequestPriority priority):
	BookmarksManagerBase( parent, priority )
{

}

CommentsManager::~CommentsManager()
{
	Clear();
}

QString CommentsManager::Get(int pageIndex)
{
	return m_bookmarks.Get( pageIndex ).Text();
}

void CommentsManager::SendRequest(int pageIndex, QString text)
{
	DebugAssert( pageIndex >= 0 );

	DebugAssert( IsActual( pageIndex ) );
	if( IsActual( pageIndex ) == false ) return;

	if( Get( pageIndex ) == text ) return;

	if( Contains( pageIndex ) )
	{
		if( text.isEmpty() ) RequestRemoval( pageIndex );
		else RequestModification( pageIndex, text );
	}
	else
	{
		if( text.isEmpty() );
		else RequestAddition( pageIndex, text );
	}
}

Bookmarks CommentsManager::Validate(Bookmarks bookmarks)
{
	QHash<int,Bookmark> result;

	foreach( Bookmark b, bookmarks )
	{
		int page = b.PageIndex();
		if( result.contains( page ) )
		{
			if( b.IsOlderThan( result.value( page ) ) ) result.insert( page, b );
		}
		else
		{
			result.insert( page, b );
		}
	}

	return result.values();
}

Bookmark CommentsManager::Validate(Bookmark bookmark)
{
	return bookmark;
}

void CommentsManager::EmitUpdate()
{
	emit SignalUpdate( this );
}

void CommentsManager::EmitLoaded()
{
    emit SignalLoaded( this );
}

void CommentsManager::SlotFinishedModificationStep1(ErrorCode error, Bookmark result)
{
	if( IsEmpty() ) return;

	BookmarksAddRequest* s = static_cast<BookmarksAddRequest*>( sender() );
	if( m_addRequests.contains( s ) == false ) return;

	if( error != API_NO_ERROR )
	{
		s->Send();
		return;
	}

	m_addRequests.remove( s );
	m_addPages.remove( s->PageIndex() );
	DeleteLaterAndNull( s );

	Bookmark oldBookmark = m_bookmarks.Get( result.PageIndex() );
	m_bookmarks.Remove( oldBookmark );
	m_bookmarks.Add( Validate( result ) );

	BookmarksDeleteRequest* r = BaseRequest()->NewCommentsDelete( oldBookmark, Priority() );
	connect(r,SIGNAL(SignalFinished(ErrorCode)),this,SLOT(SlotFinishedModificationStep2(ErrorCode)));
	m_removeRequests.insert( r );
	m_removePages.insert( r->GetBookmark().PageIndex() );
	r->Send();
}

void CommentsManager::SlotFinishedModificationStep2(ErrorCode error)
{
	if( IsEmpty() ) return;

	BookmarksDeleteRequest* s = static_cast<BookmarksDeleteRequest*>( sender() );
	if( m_removeRequests.contains( s ) == false ) return;

	if( error != API_NO_ERROR )
	{
		s->Send();
		return;
	}

	m_removeRequests.remove( s );
	m_removePages.remove( s->GetBookmark().PageIndex() );
	DeleteLaterAndNull( s );

	EmitUpdate();
}

void CommentsManager::RequestModification(int pageIndex, QString text)
{
	DebugAssert( Contains( pageIndex ) );
	if( Contains( pageIndex ) == false ) return;

	BookmarksAddRequest* r = BaseRequest()->NewCommentsAdd( GetDocument(), pageIndex, text, Priority() );
	connect(r,SIGNAL(SignalFinished(ErrorCode,Bookmark)),this,SLOT(SlotFinishedModificationStep1(ErrorCode,Bookmark)));
	connect(r,SIGNAL(SignalError()),this,SLOT(SlotErrorAddRequest()));
	m_addRequests.insert( r );
	m_addPages.insert( pageIndex );
	r->Send();
	EmitUpdate();
}

BookmarksGetRequest*CommentsManager::CreateGetRequest()
{
	return BaseRequest()->NewCommentsGetForAllPages( GetDocument(), Priority() );
}

BookmarksAddRequest*CommentsManager::CreateAddRequest(int pageIndex, QString text)
{
	DebugAssert( text.isEmpty() == false );
	return BaseRequest()->NewCommentsAdd( GetDocument(), pageIndex, text, Priority() );
}

BookmarksDeleteRequest*CommentsManager::CreateRemoveRequest(int pageIndex)
{
	return BaseRequest()->NewCommentsDelete( m_bookmarks.Get( pageIndex ), Priority() );
}
